import { TpAngularCliPage } from './app.po';

describe('tp-angular-cli App', function() {
  let page: TpAngularCliPage;

  beforeEach(() => {
    page = new TpAngularCliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
