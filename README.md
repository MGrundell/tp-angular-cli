# TpAngularCli

  Project based on angular-CLI.


## Install project

  In order to run project,
      - Install Node v6.0.0+
      - Install Angular-CLI with -g flag
            (make sure its the latest version)

      - In project root folder run: npm install

      - Start server:
          Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

      - Create production build:
          Run `ng build -prod`


## Project structure

      Modules
          Responsible to load dependencies for application on different levels

      Routes
          Handles routing for the application

      Components
          Handles the data / input etc. of each "page"

      Service
          Handles http requests

      Models (data models)
          Holds data structure to match backend data structure


      Each component has a templateUrl / styleUrl linked to it.  
