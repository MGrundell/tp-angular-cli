import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule }   from '@angular/router';

import { SearchComponent }     from './search.component';

const SearchRouting: Routes = [
  {
    path: 'search/:searchTerm',
    component: SearchComponent
  }
];

export const SearchRoutes: ModuleWithProviders = RouterModule.forChild(SearchRouting);
