import {Injectable} from '@angular/core';
import {URLSearchParams, Jsonp } from '@angular/http';

@Injectable()

export class SearchService {
  private userInfo = JSON.parse(localStorage.getItem('auth_key'));

  constructor(
      private _jsonp: Jsonp
  ){}

  search (term: string) {
    var search = new URLSearchParams()
    search.set('query', term);
    if(this.userInfo)
    { search.set('requestingUserId', this.userInfo.id)};
    search.set('callback', 'JSONP_CALLBACK');

    console.log(search);
    return this._jsonp
                .get('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9005/search-service/search2', { search })
                .map((response) => response.json());
  }
}
