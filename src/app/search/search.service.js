"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var SearchService = (function () {
    function SearchService(_jsonp) {
        this._jsonp = _jsonp;
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
    }
    SearchService.prototype.search = function (term) {
        var search = new http_1.URLSearchParams();
        search.set('query', term);
        if (this.userInfo) {
            search.set('requestingUserId', this.userInfo.id);
        }
        ;
        search.set('callback', 'JSONP_CALLBACK');
        console.log(search);
        return this._jsonp
            .get('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9005/search-service/search2', { search: search })
            .map(function (response) { return response.json(); });
    };
    SearchService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Jsonp])
    ], SearchService);
    return SearchService;
}());
exports.SearchService = SearchService;
