"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
require('rxjs/add/operator/debounceTime');
require('rxjs/add/operator/distinctUntilChanged');
require('rxjs/add/operator/switchMap');
var Photo = (function () {
    function Photo(_http) {
        this._http = _http;
        this.API_KEY = "cc5c5a1be4c7fa1a296a391d2575e0c9";
        this.baseUrl = 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com';
    }
    Photo.prototype.photoSearch = function (term) {
        return this._http.get('https://api.flickr.com/services/rest/?method=flickr.photos.search&&api_key=' + this.API_KEY + '&tags=' + term + '&accuracy=6&per_page=5&format=json&nojsoncallback=1')
            .map(function (data) { return data.json().photos.reduce(function (all, photo) { return all.concat(photo); }, []); });
    };
    Photo.prototype.searchFlickrPhotos = function (term, location) {
        console.log('search Term FLICKR', term);
        if (term) {
            return this._http.get('https://api.flickr.com/services/rest/?method=flickr.photos.search&&api_key=' + this.API_KEY + '&tags=' + location + '&text=' + term + '&accuracy=11&per_page=5&format=json&nojsoncallback=1')
                .map(function (data) { return data.json().photos.photo; })
                .flatMap(function (single) { return single; })
                .map(function (single) { return 'https://farm' + single.farm + '.staticflickr.com/' + single.server + '/' + single.id + '_' + single.secret + '_q.jpg'; })
                .reduce(function (a, b) { return a.concat(b); }, []);
        }
    };
    Photo = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], Photo);
    return Photo;
}());
exports.Photo = Photo;
/*
return this._http.post('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service/getSuggestedBoardTagsJSON', body, {headers: headers})
.map(data => data.json()); */
/*return this._http.get('https://api.flickr.com/services/rest/?method=flickr.photos.search&&api_key=' +this.API_KEY+'&tags='+ term+'&accuracy=6&per_page=5&format=json&nojsoncallback=1')
.map(data => {
    data.json().photos.photo
    console.log(data.json().photos.photo)
}) */
/*


var url = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&&api_key='
            +this.API_KEY+'&tags='+ term+'&accuracy=6&per_page=5&format=json&nojsoncallback=1';
this.photos = [];
return this._http.get(url)
  .map(res => res.json())
  .subscribe(
    data => {
      console.log(data)
        data.photos.photo.forEach(photo => {
            this.photos.push(
              new Photo(photo.id,photo.server,photo.farm,photo.secret
            ))
        })

    },
    err => {
      console.log(err)
    }
  );

}
*/
