"use strict";
var search_component_1 = require('./search.component');
exports.SearchRoutes = [
    {
        path: 'search/:searchTerm',
        component: search_component_1.SearchComponent
    }
];
