import {Component, Pipe, PipeTransform, OnInit} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router'
import { Jsonp } from '@angular/http';

//import {Control} from '@angular/common';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import {SearchService} from './search.service';

@Component({
  selector: 'search-component',
  templateUrl: 'search.component.html',
  styleUrls: ['../boards/board.component.css'],
})

export class SearchComponent implements OnInit {
  items: Observable<Array<any>>;
  searchTerm: string;
  //term = new Control();
  searchResults: any = [];

  private sub: any;
  value = 'default';

  constructor(
    private _searchService: SearchService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {  }

  gotoDetail($event) {
      console.log($event);
      this._router.navigate(['/board', $event.id]);
    }

  goToUser($event){
      console.log($event);
      this._router.navigate(['/user', $event]);
    }

  inc(value: string){
    this.value = value;
  }

  ngOnInit() {
    this.sub = this._route.params.subscribe(params => {
      let searchVal = this.searchTerm = params['searchTerm'];
      this._searchService.search(searchVal).subscribe(
        searchResults =>{
          console.log(searchResults)
          this.searchResults = searchResults
        }
      )
    });
  }
}
