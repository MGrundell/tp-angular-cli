import {Component,  OnInit, Input, Output, ElementRef, EventEmitter} from '@angular/core';
//import {CORE_DIRECTIVES} from '@angular/common';
import { Router} from '@angular/router';

import {Observable} from 'rxjs/Rx';
import {AuthService} from '../auth/auth.service';


@Component({
  // moduleId: module.id,
  selector: 'input-debounce',
  templateUrl: 'search-input.component.html',
  styleUrls: ['search-input.component.css']
})
export class InputDebounceComponent implements OnInit {

  public inputValue: string;
  public Location: string;

  constructor(
    private _elRef: ElementRef,
    private _authService: AuthService,
    private _router: Router
  ) {

    const eventStream = Observable.fromEvent(_elRef.nativeElement, 'keyup')
            .map(() => this.inputValue)
            .debounceTime(this.delay)
            .distinctUntilChanged();
        eventStream.subscribe(input => this.value.emit(input));
    }

  @Input() placeholder: string;
  @Input() delay: number = 300;
  @Output() value: EventEmitter<any> = new EventEmitter();

  ngOnInit() {}

  isAuth(){
      return this._authService.isAuthenticated();
    }

    signUp(){
      this._router.navigate(['/signUp']);
     console.log('false');
    }
    createBoard(){
      this._router.navigate(['/create-board']);
    }
}
