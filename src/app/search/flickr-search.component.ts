import { Component, EventEmitter, Input, Output, OnInit, OnDestroy } from '@angular/core';
import {Http} from '@angular/http';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  FormArray
} from "@angular/forms";

//import {Control} from '@angular/common';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import {SEMANTIC_COMPONENTS, SEMANTIC_DIRECTIVES} from 'ng-semantic/ng-semantic'

import {Photo} from './flickr-search.service';

@Component({
  selector: 'flickr-search',
  templateUrl: 'flickr-search.component.html',
  outputs: ['photoSelected'],
  styles: [
    `.selected {
      border: 3px solid green;
    }`
  ]
})

export class FlickrPhotoComponent implements OnInit {
  selected: string;
  searchStr: string = "";
  API_KEY: string = "cc5c5a1be4c7fa1a296a391d2575e0c9";
  photos:Array<any> = [];
  photoAdded: string = '';
  isOn = false;
  isDisabled = false;

  flickrImgSearch: FormGroup;

  flickrInput: FormControl = new FormControl("");

  photoSelected = new EventEmitter<string>();

  photoItems: Observable<void>;

  constructor(
    public _http:Http,
    private _formBuilder: FormBuilder,
    private _photo: Photo,
    public id:string,
    public server:string,
    public farm:string,
    public secret:string
  ) {

      this.flickrImgSearch = _formBuilder.group({
      'searchVal': this.flickrInput,
      });
      console.log('flickrInput', this.flickrInput);
      this.flickrInput.valueChanges
                     .debounceTime(400)
                     .distinctUntilChanged()
                     .switchMap(term => this._photo.photoSearch(term));

    /*this.flickrImgSearch.valueChanges.subscribe(
        (data: any) => console.log(data.searchVal)
      ); */


    }

  searchPhoto(){

}

  addPhotoToBoard(photoAdded: string){
      this.photoAdded = photoAdded;
      this.photoSelected.emit(photoAdded);
    }


  ngOnInit() {}
}
