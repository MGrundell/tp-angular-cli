import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';


export interface flickerResult{
   id: string;
   farm: string;
   server: string;
   secret: string;
};

@Injectable()

export class Photo{
    API_KEY: string = "cc5c5a1be4c7fa1a296a391d2575e0c9";
    baseUrl = 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com';
    constructor(

      private _http: Http
    ){

    }

    photoSearch(term){
      return this._http.get('https://api.flickr.com/services/rest/?method=flickr.photos.search&&api_key='+this.API_KEY+'&tags='+ term+'&accuracy=6&per_page=5&format=json&nojsoncallback=1')
      .map(data => data.json().photos.reduce(
        (all, photo) => all.concat(photo), [])
      )
    }

    searchFlickrPhotos(term){
      console.log('search Term FLICKR', term);

      if(term){
        return this._http.get("https://api.flickr.com/services/rest/?method=flickr.photos.search&&api_key=" +
                    "cc5c5a1be4c7fa1a296a391d2575e0c9&tags="+ term+"&accuracy=11&per_page=5&format=json&nojsoncallback=1")
                    .map(data => data.json().photos.photo)
                    .flatMap(single => single)
                    .map((single : {farm: string, id: string, secret: string, server: string}) => {
                        return "https://farm" + single.farm + ".staticflickr.com/" + single.server + "/"
                            + single.id + "_" + single.secret + "_q.jpg";
                    })
                    .reduce((a: string[], b: string) => [...a, b], []);
      }
    }
}


/*
return this._http.post('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service/getSuggestedBoardTagsJSON', body, {headers: headers})
.map(data => data.json()); */
/*return this._http.get('https://api.flickr.com/services/rest/?method=flickr.photos.search&&api_key=' +this.API_KEY+'&tags='+ term+'&accuracy=6&per_page=5&format=json&nojsoncallback=1')
.map(data => {
    data.json().photos.photo
    console.log(data.json().photos.photo)
}) */




/*


var url = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&&api_key='
            +this.API_KEY+'&tags='+ term+'&accuracy=6&per_page=5&format=json&nojsoncallback=1';
this.photos = [];
return this._http.get(url)
  .map(res => res.json())
  .subscribe(
    data => {
      console.log(data)
        data.photos.photo.forEach(photo => {
            this.photos.push(
              new Photo(photo.id,photo.server,photo.farm,photo.secret
            ))
        })

    },
    err => {
      console.log(err)
    }
  );

}
*/
