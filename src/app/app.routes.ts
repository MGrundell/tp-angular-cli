import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { BoardComponent} from './boards/board.component';
import { OtherComponent} from './other/other.component';
import { BoardSingleComponent} from './boards/single/board-single.component';
import { BoardRoutes }        from './boards/board.routes';
import { TagRoutes} from './tags/tag.routes';
import { SearchRoutes } from './search/search.routes';
import { load } from './async-ng-module-loader';
import { UserModule } from './users/user.module';
import { TagComponent} from './tags/tag.component';
import { SearchComponent } from './search/search.component';

const UserRouting : Routes = [
   {
     path: 'user/:id',
     loadChildren: load(() => new Promise(resolve => {
         (require as any).ensure([], require => {
           resolve(require('./users/user.module').UserModule);
         })
       }))
   }
  //  {
  //     path: "user/:id",
  //     loadChildren: "app/users/user.module#UserModule"
  //   }
];

const TagRuting : Routes = [
  {
    path: 'tag/:tagId',
    component: TagComponent
  }
]

const searchRuting : Routes = [
  {
    path: 'search/:searchTerm',
    component: SearchComponent
  }
]

const APP_ROUTES : Routes = [
//  ...BoardRoutes,
  ...UserRouting,
  ...TagRuting,
  ...searchRuting
//  ...TagRoutes,
//  ...SearchRoutes

];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders  = RouterModule.forRoot(APP_ROUTES);
