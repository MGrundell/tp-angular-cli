import { Injectable } from '@angular/core';
//import { URLSearchParams, Jsonp} from '@angular/http';
import { Board} from '../boards/board'
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { URLSearchParams, Jsonp} from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class BoardService {
  private userInfo = JSON.parse(localStorage.getItem('auth_key'));
  private baseUrl: string;

  private _boards$: Subject<Board[]>;
  private dataStore: {  // This is where we will store our data in memory
    boards: Board[]
  };
  private _following$: Subject<Board[]>;
  private followingStore: {
     // This is where we will store our data in memory
    boards: Board[]
  };
  constructor(
    private _http: Http,
    private jsonp: Jsonp,
    private _router: Router
  ) {
      this.baseUrl  = 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com';

      this.dataStore = { boards: [] };
      this._boards$ = <Subject<Board[]>>new Subject();

      this.followingStore = { boards: [] };
      this._following$ = <Subject<Board[]>>new Subject();
     }


    get board$() {
       return this._boards$.asObservable();
    }

    get following$(){
      return this._boards$.asObservable();
    }

    getData(){
      this._http.get(`${this.baseUrl}:9000/board-service/boards`).map(response => response.json()).subscribe(data => {
        this.dataStore.boards = data;
        this._boards$.next(this.dataStore.boards);
        console.log(this.dataStore.boards);
      },
      error => console.log('Could not load boards.'));
    }

    createBoard(data){
      console.log(data);
      const body = JSON.stringify(data);
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log('Passed data crate board:', body);
      return this._http.post('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service/addBoardJSON', body, {headers: headers})
      .map(data => data.json())
      .subscribe(
        data => {
          console.log('new Board Created', data);
          this._router.navigate(['/board', data.id]);
        },
        error => console.log(error)
      );
    }

    getBoardDetailed(boardId){
      console.log('Using stream board:', boardId);
      if(this.userInfo){
       this._http.get(`${this.baseUrl}:9000/board-service?boardId=${boardId}&requestingUserId=${this.userInfo.id}`)
       .map(response => response.json())
       .subscribe(
          data => {
            console.log('The detailed Board JSON data:', data);
            this.dataStore.boards = [data];
            this._boards$.next(this.dataStore.boards);
          },
          error => console.log('Could not load board.')
        );
        }

        if(!this.userInfo){
         this._http.get(`${this.baseUrl}:9000/board-service?boardId=${boardId}`)
         .map(response => response.json())
         .subscribe(
            data => {
              console.log('The detailed Board JSON data:', data);
              this.dataStore.boards = [data];
              this._boards$.next(this.dataStore.boards);
            },
            error => console.log('Could not load board.')
          );
          }
      }

    getBoardsAuth(user){
      console.log('Using tbf for:', user);
      this.dataStore.boards = [];
       this._http.get(`${this.baseUrl}:9009/algo-service/suggestBoards?algoType=TBF&email=${user}`).map(response => response.json()).subscribe(data => {
        this.dataStore.boards = data;
        this._boards$.next(this.dataStore.boards);
      },
      error => console.log('Could not load todos.'));
    }

    getBoardsByFollowing(user){
      console.log('Using tbf for:', user);
      this.dataStore.boards = [];
       this._http.get(`${this.baseUrl}:9009/algo-service/suggestBoardsBasedOnFollowing?id=${user}`).map(response => response.json()).subscribe(data => {
         this.dataStore.boards = data;
         console.log(data);
         this._boards$.next(this.dataStore.boards);
        console.log( 'Follwing Data Store', this.dataStore.boards);
      },
      error => console.log('Could not load todos.'));
    }

    getBoardsByLikes(user){
      console.log('Using tbf for:', user);
      this.dataStore.boards = [];
       this._http.get(`${this.baseUrl}:9009/algo-service/suggestBoardsBasedOnLikes?id=${user}`).map(response => response.json()).subscribe(data => {
        this.dataStore.boards = data;
        console.log(data);
        this._boards$.next(this.dataStore.boards);
        console.log(this.dataStore.boards);
      },
      error => console.log('Could not load todos.'));
    }

    addSnippet(snippet){
      console.log(snippet)
      const body = JSON.stringify(snippet);
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this._http.post(`${this.baseUrl}:9000/board-service/addBoardSnippetJSON`, body, {headers: headers})
        .map(response => response.json()).subscribe(data => {
          this.dataStore.boards.forEach((board, i) => {
            if (board.id === data.id) { this.dataStore.boards[i] = data; }
          });
        }, error => console.log('Could not update todo.'));
    }

    addHotel(term){
      console.log('Adding Hotel', term)
      const body = JSON.stringify(term);
      console.log('body:', body);
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this._http.post('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service/addHotelToBoardJSON', body, {headers: headers})
      .map(response => response.json())
      .subscribe(
        data => {
          console.log('data returned', data)
          this.dataStore.boards.forEach((board, i) => {
          console.log('data in DataStore', board);
            if (board.id === data.id) {
              this.dataStore.boards[i] = data;
            }
          });
        },
        error => console.log('Could not update todo.')
      );
    }

    likeBoard(board) {
      console.log(board)
        const body = JSON.stringify(board);
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this._http.post(`${this.baseUrl}:9000/board-service/likeBoardIdJSON`, body, {headers: headers})
          .map(response => response.json()).subscribe(data => {
            this.dataStore.boards.forEach((board, i) => {
              if (board.id === data.id) { this.dataStore.boards[i] = data; }
            });
          //  this._boards$.next(this.dataStore.boards);
          }, error => console.log('Could not update todo.'));
      }

    suggestTag(term){
      console.log(term);
      const tagSuggest = {
        textToAnalyse : term
      }
      if(!tagSuggest.textToAnalyse){
        console.log('empty s ')
      }
      if(tagSuggest.textToAnalyse){

        const body = JSON.stringify(tagSuggest);
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this._http.post('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service/getSuggestedBoardTagsJSON', body, {headers: headers})
        .map(data => data.json());
      }

    }
}
