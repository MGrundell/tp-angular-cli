import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router} from '@angular/router'
import 'rxjs/Rx';
//Services
import { BoardService } from './board.service';
import { Board } from './board';
import { AuthService } from '../auth/auth.service';
import { BoardTBFService } from './board-tbf.service';

import { Observable } from 'rxjs/Observable';
import { Subscription }   from 'rxjs/Subscription';

// Third party
import { MasonryOptions } from 'angular2-masonry';

@Component({
  selector: 'board-component',
  templateUrl: 'board.component.html',
  styleUrls: ['board.component.css'],
})

export class BoardComponent implements OnInit {
  boards: Array<any>;
  userInfo = JSON.parse(localStorage.getItem('auth_key'));
  singleBoards$: Observable<Board[]>;
  edited: Array<any> = [];
  lycky: string = '';
  boardLikeCount: number;
  newLike: number;
  confirmed = false;
  boardId: Array<any> = [];
  boards$: Observable<Board[]>;

  public sort: {field: string, desc: boolean} = {field: 'score', desc: true};
  constructor(
    private _boardService: BoardService,
    private _authService: AuthService,
    private _TBFService: BoardTBFService,
    private _router: Router

  ) { }

  public myOptions: MasonryOptions = {
    transitionDuration: '0.8s',
  };

  goToByLikes(){
    this._router.navigate(['/byLikes']);
  };

  goToByFollowing(){
    this._router.navigate(['/byFollowing']);
  };

  goToMain(){
    this._router.navigate(['/']);
  };

  gotoDetail(board: Board) {
    this._router.navigate(['/board', board.id]);
  };

  toggleDate(){
    this.sort = {field: 'createdAt', desc: false};
  };

  toggleScore() {
     this.sort = {field: 'score', desc: true};
  };

  goToUser($event) {
    this._router.navigate(['/user', $event]);
  };

  isAuth(){
    return this._authService.isAuthenticated();
  };

  likeBoard(boardId){
    const data = {
      'requestingUserId': this.userInfo.id,
      'boardId': boardId
    }
    this._TBFService.likeBoard(data)
  }

  ngOnInit() {
    // Auth call
    if (this.userInfo !== null){
      this.boards$ = this._TBFService.boards$
      this._TBFService.getBoardsAuth(this.userInfo.email);
    }
    // No Auth Service -> Calls to Board
    if(this.userInfo == null){
      console.log('Loads Without user info');
      this.boards$ = this._boardService.board$;
      this._boardService.getData();
    }
  }

  ngOnDestroy(){

  }
}
