import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router'

import {BoardService} from '../board.service';
import {Board} from '../board';
import {AuthService} from '../../auth/auth.service';
import { LikeBoardPipe } from '../board-auth.pipe';

import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'BoardFollowingComponent',
  templateUrl: '../board.component.html',
  styleUrls: ['../board.component.css'],
})
export class BoardFollowingComponent implements OnInit {
  boards: Array<any>;
  userInfo = JSON.parse(localStorage.getItem('auth_key'));
  boards$: Observable<Board[]>;
  singleBoards$: Observable<Board>;
  edited: Array<any> = [];
  public sort: {field: string, desc: boolean} = {field: 'score', desc: true};

  constructor(
      private _boardService: BoardService,
      private _router: Router,
      private _authService: AuthService,
  ) {  }

/** ROUTING **/
  goToByLikes(){
    this._router.navigate(['/byLikes']);
  }

  goToByFollowing(){
    this._router.navigate(['/byFollowing']);
  }

  goToMain(){
    this._router.navigate(['/']);
  }

  gotoDetail(board: Board) {
      this._router.navigate(['/board', board.id]);
    }

  toggleDate(){
    this.sort = {field: 'createdAt', desc: true};
  }

  toggleScore() {
     this.sort = {field: 'score', desc: true};
  }

  goToUser($event) {
    this._router.navigate(['/user', $event]);
  }


  isAuth(){
    return this._authService.isAuthenticated();
  }


  likeBoard(boardId){
        this.edited.push(boardId);
        const data = {
          'requestingUserId': this.userInfo.id,
          'boardId': boardId
        }
        this._boardService.likeBoard(data)
        console.log(this.edited);
      }

  ngOnInit() {
      this.boards$ = this._boardService.board$; // subscribe to entire collection
      this._boardService.getBoardsByFollowing(this.userInfo.id);    // load all todos

    }
}
