"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var board_no_Auth_service_1 = require('../board-no-Auth.service');
var auth_service_1 = require('../../auth/auth.service');
var board_auth_pipe_1 = require('../board-auth.pipe');
var other_service_1 = require('../../other/other.service');
var sort_pipe_1 = require('../sort.pipe');
var BoardFollowingComponent = (function () {
    function BoardFollowingComponent(_boardNoAuth, _router, _authService, _streamBoard) {
        this._boardNoAuth = _boardNoAuth;
        this._router = _router;
        this._authService = _authService;
        this._streamBoard = _streamBoard;
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
        this.edited = [];
        this.sort = { field: 'score', desc: true };
    }
    /** ROUTING **/
    BoardFollowingComponent.prototype.goToByLikes = function () {
        this._router.navigate(['/byLikes']);
    };
    BoardFollowingComponent.prototype.goToByFollowing = function () {
        this._router.navigate(['/byFollowing']);
    };
    BoardFollowingComponent.prototype.goToMain = function () {
        this._router.navigate(['/']);
    };
    BoardFollowingComponent.prototype.gotoDetail = function (board) {
        this._router.navigate(['/board', board.id]);
    };
    BoardFollowingComponent.prototype.toggleDate = function () {
        this.sort = { field: 'createdAt', desc: true };
    };
    BoardFollowingComponent.prototype.toggleScore = function () {
        this.sort = { field: 'score', desc: true };
    };
    BoardFollowingComponent.prototype.goToUser = function ($event) {
        this._router.navigate(['/user', $event]);
    };
    BoardFollowingComponent.prototype.isAuth = function () {
        return this._authService.isAuthenticated();
    };
    BoardFollowingComponent.prototype.likeBoard = function (boardId) {
        this.edited.push(boardId);
        var data = {
            'requestingUserId': this.userInfo.id,
            'boardId': boardId
        };
        this._boardNoAuth.likeBoard(data);
        console.log(this.edited);
    };
    BoardFollowingComponent.prototype.ngOnInit = function () {
        this.boards$ = this._boardNoAuth.board$; // subscribe to entire collection
        this._boardNoAuth.getBoardsByFollowing(this.userInfo.id); // load all todos
    };
    BoardFollowingComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'BoardFollowingComponent',
            templateUrl: '../board.component.html',
            styleUrls: ['../board.component.css'],
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [board_no_Auth_service_1.BoardNoAuthService, other_service_1.StreamBoardService],
            pipes: [board_auth_pipe_1.LikeBoardPipe, sort_pipe_1.SortyPipe]
        }), 
        __metadata('design:paramtypes', [board_no_Auth_service_1.BoardNoAuthService, router_1.Router, auth_service_1.AuthService, other_service_1.StreamBoardService])
    ], BoardFollowingComponent);
    return BoardFollowingComponent;
}());
exports.BoardFollowingComponent = BoardFollowingComponent;
