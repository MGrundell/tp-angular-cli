"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var http_2 = require('@angular/http');
var router_1 = require('@angular/router');
var Subject_1 = require('rxjs/Subject');
require('rxjs/add/operator/map');
require('rxjs/add/operator/debounceTime');
require('rxjs/add/operator/distinctUntilChanged');
require('rxjs/add/operator/switchMap');
var BoardNoAuthService = (function () {
    function BoardNoAuthService(_http, jsonp, _router) {
        this._http = _http;
        this.jsonp = jsonp;
        this._router = _router;
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
        this.baseUrl = 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com';
        this.dataStore = { boards: [] };
        this._boards$ = new Subject_1.Subject();
        this.followingStore = { boards: [] };
        this._following$ = new Subject_1.Subject();
    }
    Object.defineProperty(BoardNoAuthService.prototype, "board$", {
        get: function () {
            return this._boards$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BoardNoAuthService.prototype, "following$", {
        get: function () {
            return this._boards$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    BoardNoAuthService.prototype.getData = function () {
        var _this = this;
        this._http.get(this.baseUrl + ":9000/board-service/boards").map(function (response) { return response.json(); }).subscribe(function (data) {
            _this.dataStore.boards = data;
            _this._boards$.next(_this.dataStore.boards);
            console.log(_this.dataStore.boards);
        }, function (error) { return console.log('Could not load boards.'); });
    };
    BoardNoAuthService.prototype.createBoard = function (data) {
        var _this = this;
        console.log(data);
        var body = JSON.stringify(data);
        var headers = new http_2.Headers();
        headers.append('Content-Type', 'application/json');
        console.log('Passed data crate board:', body);
        return this._http.post('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service/addBoardJSON', body, { headers: headers })
            .map(function (data) { return data.json(); })
            .subscribe(function (data) {
            console.log('new Board Created', data);
            _this._router.navigate(['/board', data.id]);
        }, function (error) { return console.log(error); });
    };
    BoardNoAuthService.prototype.getBoardDetailed = function (boardId) {
        var _this = this;
        console.log('Using stream board:', boardId);
        if (this.userInfo) {
            this._http.get(this.baseUrl + ":9000/board-service?boardId=" + boardId + "&requestingUserId=" + this.userInfo.id)
                .map(function (response) { return response.json(); })
                .subscribe(function (data) {
                console.log('The detailed Board JSON data:', data);
                _this.dataStore.boards = [data];
                _this._boards$.next(_this.dataStore.boards);
            }, function (error) { return console.log('Could not load board.'); });
        }
        if (!this.userInfo) {
            this._http.get(this.baseUrl + ":9000/board-service?boardId=" + boardId)
                .map(function (response) { return response.json(); })
                .subscribe(function (data) {
                console.log('The detailed Board JSON data:', data);
                _this.dataStore.boards = [data];
                _this._boards$.next(_this.dataStore.boards);
            }, function (error) { return console.log('Could not load board.'); });
        }
    };
    BoardNoAuthService.prototype.getBoardsAuth = function (user) {
        var _this = this;
        console.log('Using tbf for:', user);
        this.dataStore.boards = [];
        this._http.get(this.baseUrl + ":9009/algo-service/suggestBoards?algoType=TBF&email=" + user).map(function (response) { return response.json(); }).subscribe(function (data) {
            _this.dataStore.boards = data;
            _this._boards$.next(_this.dataStore.boards);
        }, function (error) { return console.log('Could not load todos.'); });
    };
    BoardNoAuthService.prototype.getBoardsByFollowing = function (user) {
        var _this = this;
        console.log('Using tbf for:', user);
        this.dataStore.boards = [];
        this._http.get(this.baseUrl + ":9009/algo-service/suggestBoardsBasedOnFollowing?id=" + user).map(function (response) { return response.json(); }).subscribe(function (data) {
            _this.dataStore.boards = data;
            console.log(data);
            _this._boards$.next(_this.dataStore.boards);
            console.log('Follwing Data Store', _this.dataStore.boards);
        }, function (error) { return console.log('Could not load todos.'); });
    };
    BoardNoAuthService.prototype.getBoardsByLikes = function (user) {
        var _this = this;
        console.log('Using tbf for:', user);
        this.dataStore.boards = [];
        this._http.get(this.baseUrl + ":9009/algo-service/suggestBoardsBasedOnLikes?id=" + user).map(function (response) { return response.json(); }).subscribe(function (data) {
            _this.dataStore.boards = data;
            console.log(data);
            _this._boards$.next(_this.dataStore.boards);
            console.log(_this.dataStore.boards);
        }, function (error) { return console.log('Could not load todos.'); });
    };
    BoardNoAuthService.prototype.addSnippet = function (snippet) {
        var _this = this;
        console.log(snippet);
        var body = JSON.stringify(snippet);
        var headers = new http_2.Headers();
        headers.append('Content-Type', 'application/json');
        this._http.post(this.baseUrl + ":9000/board-service/addBoardSnippetJSON", body, { headers: headers })
            .map(function (response) { return response.json(); }).subscribe(function (data) {
            _this.dataStore.boards.forEach(function (board, i) {
                if (board.id === data.id) {
                    _this.dataStore.boards[i] = data;
                }
            });
        }, function (error) { return console.log('Could not update todo.'); });
    };
    BoardNoAuthService.prototype.addHotel = function (term) {
        var _this = this;
        console.log('Adding Hotel', term);
        var body = JSON.stringify(term);
        console.log('body:', body);
        var headers = new http_2.Headers();
        headers.append('Content-Type', 'application/json');
        this._http.post('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service/addHotelToBoardJSON', body, { headers: headers })
            .map(function (response) { return response.json(); })
            .subscribe(function (data) {
            console.log('data returned', data);
            _this.dataStore.boards.forEach(function (board, i) {
                console.log('data in DataStore', board);
                if (board.id === data.id) {
                    _this.dataStore.boards[i] = data;
                }
            });
        }, function (error) { return console.log('Could not update todo.'); });
    };
    BoardNoAuthService.prototype.likeBoard = function (board) {
        var _this = this;
        console.log(board);
        var body = JSON.stringify(board);
        var headers = new http_2.Headers();
        headers.append('Content-Type', 'application/json');
        this._http.post(this.baseUrl + ":9000/board-service/likeBoardIdJSON", body, { headers: headers })
            .map(function (response) { return response.json(); }).subscribe(function (data) {
            _this.dataStore.boards.forEach(function (board, i) {
                if (board.id === data.id) {
                    _this.dataStore.boards[i] = data;
                }
            });
            //  this._boards$.next(this.dataStore.boards);
        }, function (error) { return console.log('Could not update todo.'); });
    };
    BoardNoAuthService.prototype.suggestTag = function (term) {
        console.log(term);
        var tagSuggest = {
            textToAnalyse: term
        };
        if (!tagSuggest.textToAnalyse) {
            console.log('empty s ');
        }
        if (tagSuggest.textToAnalyse) {
            var body = JSON.stringify(tagSuggest);
            var headers = new http_2.Headers();
            headers.append('Content-Type', 'application/json');
            return this._http.post('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service/getSuggestedBoardTagsJSON', body, { headers: headers })
                .map(function (data) { return data.json(); });
        }
    };
    BoardNoAuthService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_2.Http, http_1.Jsonp, router_1.Router])
    ], BoardNoAuthService);
    return BoardNoAuthService;
}());
exports.BoardNoAuthService = BoardNoAuthService;
