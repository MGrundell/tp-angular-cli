"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var http_2 = require('@angular/http');
var Subject_1 = require('rxjs/Subject');
require('rxjs/add/operator/map');
require('rxjs/add/operator/debounceTime');
require('rxjs/add/operator/distinctUntilChanged');
require('rxjs/add/operator/switchMap');
var BoardTBFService = (function () {
    function BoardTBFService(_http, jsonp) {
        this._http = _http;
        this.jsonp = jsonp;
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
        this.baseUrl = 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com';
        this.dataStore = { boards: [] };
        this._boards$ = new Subject_1.Subject();
    }
    Object.defineProperty(BoardTBFService.prototype, "boards$", {
        get: function () {
            return this._boards$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    BoardTBFService.prototype.getBoardsAuth = function (user) {
        var _this = this;
        console.log('Using tbf for:', user);
        this._http.get(this.baseUrl + ":9009/algo-service/suggestBoards?algoType=TBF&email=" + user)
            .map(function (response) { return response.json(); })
            .subscribe(function (data) {
            console.log('Data passed getBoardsAuth', data);
            _this.dataStore.boards = data;
            _this._boards$.next(_this.dataStore.boards);
        }, function (error) { return console.log('Could not load Boards.'); });
    };
    BoardTBFService.prototype.getBoards = function (tagId) {
        var _this = this;
        console.log('calling board by tag', tagId);
        this._http.get(this.baseUrl + ":9000/board-service/taggedBoards?tagId=" + tagId + "&requestingUserId=" + this.userInfo.id)
            .map(function (response) { return response.json(); })
            .subscribe(function (data) {
            console.log('Data passed getBoardsAuth', data);
            _this.dataStore.boards = data;
            _this._boards$.next(_this.dataStore.boards);
        }, function (error) { return console.log('Could not load Boards.'); });
    };
    BoardTBFService.prototype.likeBoard = function (board) {
        var _this = this;
        console.log(board);
        var body = JSON.stringify(board);
        var headers = new http_2.Headers();
        headers.append('Content-Type', 'application/json');
        this._http.post(this.baseUrl + ":9000/board-service/likeBoardIdJSON", body, { headers: headers })
            .map(function (response) { return response.json(); }).subscribe(function (data) {
            console.log('Data returned', data);
            _this.dataStore.boards.forEach(function (board, i) {
                if (board.id === data.id) {
                    _this.dataStore.boards[i] = data;
                    console.log(_this.dataStore.boards[i]);
                }
            });
            _this._boards$.next(_this.dataStore.boards);
            console.log('liked');
        }, function (error) { return console.log('Could not update The like.'); });
    };
    BoardTBFService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_2.Http, http_1.Jsonp])
    ], BoardTBFService);
    return BoardTBFService;
}());
exports.BoardTBFService = BoardTBFService;
