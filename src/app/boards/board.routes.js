"use strict";
var board_component_1 = require('./board.component');
var board_single_component_1 = require('./single/board-single.component');
var board_create_component_1 = require('./create/board-create.component');
var board_following_component_1 = require('./auth/board-following.component');
var board_byLikes_component_1 = require('./auth/board-byLikes.component');
var user_signup_component_1 = require('../users/singup/user-signup.component');
exports.BoardRoutes = [
    { path: '', component: board_component_1.BoardComponent },
    { path: 'board/:id', component: board_single_component_1.BoardSingleComponent },
    { path: 'create-board', component: board_create_component_1.BoardCreateComponent },
    { path: 'byFollowing', component: board_following_component_1.BoardFollowingComponent },
    { path: 'byLikes', component: board_byLikes_component_1.BoardByLikesComponent },
    { path: 'signUp', component: user_signup_component_1.UserSignup }
];
