import { Injectable } from '@angular/core';
import { URLSearchParams, Jsonp} from '@angular/http';
import { Board } from '../boards/board'
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class BoardTBFService {

  private baseUrl: string;
  public userInfo = JSON.parse(localStorage.getItem('auth_key'));

  private _boards$: Subject<Board[]>;
  private dataStore: {  // This is where we will store our data in memory
    boards: Board[]
  };

  constructor(
    private _http: Http,
    private jsonp: Jsonp
  ) {
      this.baseUrl  = 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com';

      this.dataStore = { boards: [] };
      this._boards$ = <Subject<Board[]>>new Subject();
     }

     get boards$() {
        return this._boards$.asObservable();
     }

     getBoardsAuth(user){

       console.log('Using tbf for:', user);
        this._http.get(`${this.baseUrl}:9009/algo-service/suggestBoards?algoType=TBF&email=${user}`)
          .map(response => response.json())
          .subscribe(
            data => {
              console.log('Data passed getBoardsAuth', data);
              this.dataStore.boards = data;
              this._boards$.next(this.dataStore.boards);
            },
            error => console.log('Could not load Boards.')
          );
     }

     getBoards(tagId: string){

       console.log('calling board by tag', tagId);
       if(this.userInfo){
        this._http.get(`${this.baseUrl}:9000/board-service/taggedBoards?tagId=${tagId}&requestingUserId=${this.userInfo.id}`)
          .map(response => response.json())
          .subscribe(
            data => {
              console.log('Data passed getBoardsAuth', data);
              this.dataStore.boards = data;
              this._boards$.next(this.dataStore.boards);
            },
            error => console.log('Could not load Boards.')
          );
     }
     if(this.userInfo == null){
       this._http.get( ` ${this.baseUrl}:9000/board-service/taggedBoards?tagId=${tagId}`)
         .map(response => response.json())
         .subscribe(
           data => {
             console.log('Data passed getBoardsAuth', data);
             this.dataStore.boards = data;
             this._boards$.next(this.dataStore.boards);
           },
           error => console.log('Could not load Boards.')
         );
     }
   }
     likeBoard(board) {
       console.log(board)
       const body = JSON.stringify(board);
       const headers = new Headers();
       headers.append('Content-Type', 'application/json');
       this._http.post(`${this.baseUrl}:9000/board-service/likeBoardIdJSON`, body, {headers: headers})
         .map(response => response.json()).subscribe(data => {
           console.log('Data returned', data);
           this.dataStore.boards.forEach((board, i) => {
             if (board.id === data.id) {
               this.dataStore.boards[i] = data;
               console.log(this.dataStore.boards[i]);
              }
           });
           this._boards$.next(this.dataStore.boards);
           console.log('liked');
         }, error => console.log('Could not update The like.'));
     }
  }
