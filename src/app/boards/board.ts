import {Tag} from '../tags/tags';
import {User} from '../users/users';

export interface Board {
  id: string;
  boardShortName: string;
  boardDescription: string;
  mainImageLink: string;
  mainHotelLink: string;
  createdByEmail: string;
  about: Tag[];
  createdBy: User[];
  likes: User[];
}
