"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
require('rxjs/Rx');
//Services
var board_no_Auth_service_1 = require('./board-no-Auth.service');
var auth_service_1 = require('../auth/auth.service');
var board_auth_pipe_1 = require('./board-auth.pipe');
var board_tbf_service_1 = require('./board-tbf.service');
var other_service_1 = require('../other/other.service');
var sort_pipe_1 = require('./sort.pipe');
var ng_semantic_1 = require('ng-semantic/ng-semantic');
var BoardComponent = (function () {
    function BoardComponent(_boardNoAuth, _router, _authService, _streamBoard, _TBFService) {
        this._boardNoAuth = _boardNoAuth;
        this._router = _router;
        this._authService = _authService;
        this._streamBoard = _streamBoard;
        this._TBFService = _TBFService;
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
        this.edited = [];
        this.lycky = '';
        this.confirmed = false;
        this.boardId = [];
        this.sort = { field: 'score', desc: true };
    }
    BoardComponent.prototype.goToByLikes = function () {
        this._router.navigate(['/byLikes']);
    };
    ;
    BoardComponent.prototype.goToByFollowing = function () {
        this._router.navigate(['/byFollowing']);
    };
    ;
    BoardComponent.prototype.goToMain = function () {
        this._router.navigate(['/']);
    };
    ;
    BoardComponent.prototype.gotoDetail = function (board) {
        this._router.navigate(['/board', board.id]);
    };
    ;
    BoardComponent.prototype.toggleDate = function () {
        this.sort = { field: 'createdAt', desc: false };
    };
    ;
    BoardComponent.prototype.toggleScore = function () {
        this.sort = { field: 'score', desc: true };
    };
    ;
    BoardComponent.prototype.goToUser = function ($event) {
        this._router.navigate(['/user', $event]);
    };
    ;
    BoardComponent.prototype.isAuth = function () {
        return this._authService.isAuthenticated();
    };
    ;
    BoardComponent.prototype.boardIncrement = function (likeCount, boardId) {
        /* const  data = { 'boardId': boardId };
        this.boardId.push(data);
        this.boardLikeCount = likeCount
        this.newLike = this.boardLikeCount++ ;
        console.log(this.newLike); */
    };
    BoardComponent.prototype.likeBoard = function (boardId) {
        var data = {
            'requestingUserId': this.userInfo.id,
            'boardId': boardId
        };
        this._TBFService.likeBoard(data);
    };
    BoardComponent.prototype.ngOnInit = function () {
        // No Auth Service -> Calls to Board
        if (this.userInfo == null) {
            this.boards$ = this._boardNoAuth.board$;
            this._boardNoAuth.getData();
        }
        // Auth call
        if (this.userInfo !== null) {
            this.boards$ = this._TBFService.boards$;
            this._TBFService.getBoardsAuth(this.userInfo.email);
        }
    };
    BoardComponent.prototype.ngOnDestroy = function () {
        //  this.boards$.unsubscribe();
    };
    BoardComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'board-component',
            templateUrl: 'board.component.html',
            styleUrls: ['board.component.css'],
            directives: [ng_semantic_1.SEMANTIC_COMPONENTS, ng_semantic_1.SEMANTIC_DIRECTIVES],
            providers: [board_no_Auth_service_1.BoardNoAuthService, board_tbf_service_1.BoardTBFService, other_service_1.StreamBoardService],
            pipes: [board_auth_pipe_1.LikeBoardPipe, sort_pipe_1.SortyPipe]
        }), 
        __metadata('design:paramtypes', [board_no_Auth_service_1.BoardNoAuthService, router_1.Router, auth_service_1.AuthService, other_service_1.StreamBoardService, board_tbf_service_1.BoardTBFService])
    ], BoardComponent);
    return BoardComponent;
}());
exports.BoardComponent = BoardComponent;
