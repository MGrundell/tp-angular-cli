import {Component, Pipe, EventEmitter, PipeTransform, ElementRef ,OnInit} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  FormArray
} from "@angular/forms";

import {Hotel} from '../../hotels/hotel.model';

//import {Control} from '@angular/common';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import {AuthService} from '../../auth/auth.service';


import {BoardService} from '../board.service';
import {TagService} from '../../tags/tag.service';
import {HotelService} from '../../hotels/hotel.service';
import {Board} from '../board';
import {FlickrPhotoComponent} from '../../search/flickr-search.component';

import {Photo} from '../../search/flickr-search.service'

@Component({
  selector: 'board-create',
  templateUrl: 'board-create.component.html',
  styleUrls: ['board-create.component.css'],
})


export class BoardCreateComponent implements OnInit {

  response: string;
  tags: Array<any> = [];
  userInfo = JSON.parse(localStorage.getItem('auth_key'));
  associatedTags: Array<any> =[]
  boardPhoto= "";
  //searchContorl = new Control();
  multipleData = [];

  accordionOption = {
      exclusive: true,
      on: "click"
  }
  // Suggestion arrays
  items: Observable<Array<any>>;
  flickrImg: Observable<string[]>;
  photoAdded: string = '';

  hotels: Observable<any>;

  hotelAdded = [];


  public inputValue: string;
  public Location: string;
  suggest:any;
  term: Observable<any>;
  protected emitter = new EventEmitter<string>();
  // Form Control

  boardCreate: FormGroup;

  titleControl: FormControl = new FormControl("", Validators.compose([Validators.required, Validators.minLength(3)]));

  descriptionControl: FormControl = new FormControl("", Validators.compose([Validators.required, Validators.minLength(4)]));

  locationControl: FormControl = new FormControl("", Validators.compose([Validators.required, Validators.minLength(4)]));

  tagControl: FormControl = new FormControl("");

  flickrControl: FormControl = new FormControl("",Validators.compose([Validators.required]));

  multipleControl: FormControl = new FormControl("", Validators.required);

  imgControl: FormControl = new FormControl("", Validators.compose([Validators.required, Validators.minLength(3)]));

  photos:"";


  constructor(
    private _tagService: TagService,
    private _boardService : BoardService,
    private _router : Router,
    private _elRef: ElementRef,
    private _formBuilder: FormBuilder,
    private _flickrPhoto: Photo,
    private _hotelService: HotelService
  ) {


    this.flickrImg = this.imgControl.valueChanges
               .debounceTime(400)
               .distinctUntilChanged()
               .switchMap(
                 term => this._flickrPhoto.searchFlickrPhotos(term)
             );

    console.log('Flickr location', this.locationControl.value);
    console.log('Flickr Img Search val', this.imgControl.value);



   this.items = this.titleControl.valueChanges
                   .debounceTime(400)
                   .distinctUntilChanged()
                   .switchMap(
                     term => this._boardService.suggestTag(term)
                 );


   this.hotels = this.locationControl.valueChanges
                    .debounceTime(400)
                    .distinctUntilChanged()
                    .switchMap(
                      term => this._hotelService.getHotels(term)
                    );



    this.boardCreate = _formBuilder.group({
      'boardShortName': this.titleControl,
      'boardDescription': this.descriptionControl,
      'location': this.locationControl,
      'tags': _formBuilder.array([

      ]),
      'mainImageLink': this.flickrControl,
      'userId': this.userInfo.id,
      "createdByEmail": this.userInfo.email,
      "hotels": _formBuilder.array([

      ])
    });
   }

   addPhotoToBoard(photoAdded: string){
     this.photoAdded = photoAdded;
  }

  isLoading(event){
    event.srcElement.classList.add("loading", "positive");
    event.srcElement.classList.remove("basic", "red");
  }

  ngOnInit() {
    this._tagService.getAllTags()
            .subscribe(
              data => {
                this.tags = data;
                console.log(this.tags);
              },
              error => console.log(error)
            );
  }

  onMultiple(data: Array<string>): void {
    this.multipleData = data;
  }

  onAddHotel(data): void {
    let dataToAdd = {
      "hotelId": data.hotelId,
      "hotelName": data.hotelName,
      "hotelDescription": data.hotelDescription,
      "hotelURL": data.hotelURL,
      "hotelImage": data.hotelImage,
      "hotelRating": data.hotelRating
    }
    this.hotelAdded.push(dataToAdd);
  }

  removeHotel(obj, index){
      console.log('hotel to remove',obj);
      this.hotelAdded.splice(this.hotelAdded.indexOf(obj), 1)
  }

  createBoard(){
      this.multipleData.map(
        data => {
          (<FormArray>this.boardCreate.controls['tags']).push(new FormControl(data));
        }
      );

      this.hotelAdded.map(
        data => {
        (<FormArray>this.boardCreate.controls['hotels']).push(new FormControl(data));
        }
      )

      this._boardService.createBoard(this.boardCreate.value);
  }
}
