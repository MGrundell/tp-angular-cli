"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var forms_1 = require("@angular/forms");
var common_1 = require('@angular/common');
require('rxjs/add/operator/map');
require('rxjs/add/operator/debounceTime');
require('rxjs/add/operator/distinctUntilChanged');
require('rxjs/add/operator/switchMap');
var ng_semantic_1 = require('ng-semantic/ng-semantic');
var board_no_Auth_service_1 = require('../board-no-Auth.service');
var tag_service_1 = require('../../tags/tag.service');
var hotel_service_1 = require('../../hotels/hotel.service');
var flickr_search_service_1 = require('../../search/flickr-search.service');
var BoardCreateComponent = (function () {
    function BoardCreateComponent(_tagService, _boardNoAuth, _router, _elRef, _formBuilder, _flickrPhoto, _hotelService) {
        var _this = this;
        this._tagService = _tagService;
        this._boardNoAuth = _boardNoAuth;
        this._router = _router;
        this._elRef = _elRef;
        this._formBuilder = _formBuilder;
        this._flickrPhoto = _flickrPhoto;
        this._hotelService = _hotelService;
        this.tags = [];
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
        this.associatedTags = [];
        this.boardPhoto = "";
        this.searchContorl = new common_1.Control();
        this.multipleData = [];
        this.accordionOption = {
            exclusive: true,
            on: "click"
        };
        this.photoAdded = '';
        this.hotelAdded = [];
        this.emitter = new core_1.EventEmitter();
        this.titleControl = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(3)]));
        this.descriptionControl = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(4)]));
        this.locationControl = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(4)]));
        this.tagControl = new forms_1.FormControl("");
        this.flickrControl = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required]));
        this.multipleControl = new forms_1.FormControl("", forms_1.Validators.required);
        this.imgControl = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(3)]));
        this.flickrImg = this.imgControl.valueChanges
            .debounceTime(400)
            .distinctUntilChanged()
            .switchMap(function (term) { return _this._flickrPhoto.searchFlickrPhotos(term, _this.locationControl.value); });
        console.log('Flickr location', this.locationControl.value);
        /*
        this.imgControl.valueChanges
                       .debounceTime(400)
                       .distinctUntilChanged()
                       .switchMap(
                         term => this._flickrPhoto.searchFlickrPhotos(term)
                     ).
                     subscribe(data => {
                       console.log(data);
                       this.flickrImg = [...this.flickrImg, data]
                       console.log(this.flickrImg);
                     }); */
        this.items = this.titleControl.valueChanges
            .debounceTime(400)
            .distinctUntilChanged()
            .switchMap(function (term) { return _this._boardNoAuth.suggestTag(term); });
        this.hotels = this.locationControl.valueChanges
            .debounceTime(400)
            .distinctUntilChanged()
            .switchMap(function (term) { return _this._hotelService.getHotels(term); });
        this.boardCreate = _formBuilder.group({
            'boardShortName': this.titleControl,
            'boardDescription': this.descriptionControl,
            'location': this.locationControl,
            'tags': _formBuilder.array([]),
            'mainImageLink': this.flickrControl,
            'userId': this.userInfo.id,
            "createdByEmail": this.userInfo.email,
            "hotels": _formBuilder.array([])
        });
        /*
        this.boardCreate.valueChanges.subscribe(
          (data: any) => console.log(data)
        ); */
    }
    BoardCreateComponent.prototype.addPhotoToBoard = function (photoAdded) {
        //console.log('i have added photo', photoAdded);
        this.photoAdded = photoAdded;
        //    console.log(this.photoAdded);
    };
    BoardCreateComponent.prototype.isLoading = function (event) {
        event.srcElement.classList.add("loading", "positive");
        event.srcElement.classList.remove("basic", "red");
    };
    BoardCreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._tagService.getAllTags()
            .subscribe(function (data) {
            _this.tags = data;
            console.log(_this.tags);
        }, function (error) { return console.log(error); });
    };
    BoardCreateComponent.prototype.onMultiple = function (data) {
        this.multipleData = data;
    };
    BoardCreateComponent.prototype.onAddHotel = function (data) {
        var dataToAdd = {
            "hotelId": data.hotelId,
            "hotelName": data.hotelName,
            "hotelDescription": data.hotelDescription,
            "hotelURL": data.hotelURL,
            "hotelImage": data.hotelImage,
            "hotelRating": data.hotelRating
        };
        this.hotelAdded.push(dataToAdd);
        //  console.log(data);
    };
    BoardCreateComponent.prototype.removeHotel = function (obj, index) {
        console.log('hotel to remove', obj);
        this.hotelAdded.splice(this.hotelAdded.indexOf(obj), 1);
    };
    BoardCreateComponent.prototype.createBoard = function () {
        var _this = this;
        this.multipleData.map(function (data) {
            _this.boardCreate.find('tags').push(new forms_1.FormControl(data));
        });
        this.hotelAdded.map(function (data) {
            _this.boardCreate.find('hotels').push(new forms_1.FormControl(data));
        });
        this._boardNoAuth.createBoard(this.boardCreate.value);
        /*(<FormArray>this.boardCreate.find('tagsControl')).push(new FormControl(this.multipleData)); */
        //  console.log(this.multipleData);
        //  console.log(JSON.stringify(this.boardCreate.value));
    };
    BoardCreateComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'board-create',
            templateUrl: 'board-create.component.html',
            styleUrls: ['board-create.component.css'],
            directives: [
                ng_semantic_1.SEMANTIC_COMPONENTS,
                ng_semantic_1.SEMANTIC_DIRECTIVES,
                //    FlickrPhotoComponent,
                forms_1.REACTIVE_FORM_DIRECTIVES
            ],
            providers: [
                board_no_Auth_service_1.BoardNoAuthService,
                tag_service_1.TagService,
                flickr_search_service_1.Photo,
                hotel_service_1.HotelService
            ]
        }), 
        __metadata('design:paramtypes', [tag_service_1.TagService, board_no_Auth_service_1.BoardNoAuthService, router_1.Router, core_1.ElementRef, forms_1.FormBuilder, flickr_search_service_1.Photo, hotel_service_1.HotelService])
    ], BoardCreateComponent);
    return BoardCreateComponent;
}());
exports.BoardCreateComponent = BoardCreateComponent;
