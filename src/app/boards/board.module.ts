// Angular modules
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule  } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';

// Third Party modules
import { NgSemanticModule } from 'ng-semantic';
import { MasonryModule } from 'angular2-masonry';

// Components
import { BoardComponent } from './board.component';
import { BoardSingleComponent } from './single/board-single.component';
import { BoardCreateComponent } from './create/board-create.component';
import { BoardFollowingComponent } from './auth/board-following.component';
import { BoardByLikesComponent } from './auth/board-byLikes.component';
import { FlickrPhotoComponent } from '../search/flickr-search.component';
import { UserSignup } from '../users/singup/user-signup.component';
// Services
import { BoardService } from './board.service';
import { AuthService } from '../auth/auth.service';
import { BoardTBFService } from './board-tbf.service';
import { TagService } from '../tags/tag.service';
import { Photo } from '../search/flickr-search.service';
import { HotelService } from '../hotels/hotel.service';

import { BoardRoutes } from './board.routes';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    NgSemanticModule,
    BoardRoutes,
    MasonryModule
  ],
  declarations: [
    BoardComponent,
    BoardSingleComponent,
    BoardCreateComponent,
    BoardFollowingComponent,
    BoardByLikesComponent,
    UserSignup
  ],
  providers:[
    BoardTBFService,
    AuthService,
    BoardService
  ]
})

export class BoardModule {}
