import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule }   from '@angular/router';
import { BoardComponent }     from './board.component';
import { BoardSingleComponent }   from './single/board-single.component';
import { BoardCreateComponent }  from './create/board-create.component';
import { BoardFollowingComponent } from './auth/board-following.component';
import { BoardByLikesComponent } from './auth/board-byLikes.component';
import { UserSignup  } from '../users/singup/user-signup.component';

const BoardRouting: Routes = [
  { path: '',  component: BoardComponent },
  { path: 'board/:id', component: BoardSingleComponent },
  { path: 'create-board', component: BoardCreateComponent },
  { path: 'byFollowing', component: BoardFollowingComponent },
  { path: 'byLikes', component: BoardByLikesComponent },
  { path: 'signUp', component: UserSignup }

];

export const BoardRoutes: ModuleWithProviders = RouterModule.forChild(BoardRouting);
