"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var _ = require('lodash');
var get = _.get;
var SortyPipe = (function () {
    function SortyPipe() {
    }
    SortyPipe.prototype.transform = function (input, field, desc) {
        if (desc === void 0) { desc = true; }
        if (input && field) {
            return Array.from(input).sort(function (a, b) {
                if (get(a, field) < get(b, field)) {
                    return desc ? 1 : -1;
                }
                if (get(a, field) > get(b, field)) {
                    return desc ? -1 : 1;
                }
                return 0;
            });
        }
        console.log(input);
        return input;
    };
    SortyPipe = __decorate([
        core_1.Pipe({ name: 'orderBy' }), 
        __metadata('design:paramtypes', [])
    ], SortyPipe);
    return SortyPipe;
}());
exports.SortyPipe = SortyPipe;
