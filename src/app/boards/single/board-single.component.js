"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var board_no_Auth_service_1 = require('../board-no-Auth.service');
var tag_service_1 = require('../../tags/tag.service');
var ng_semantic_1 = require('ng-semantic/ng-semantic');
var auth_service_1 = require('../../auth/auth.service');
var hotel_service_1 = require('../../hotels/hotel.service');
var forms_1 = require("@angular/forms");
var BoardSingleComponent = (function () {
    function BoardSingleComponent(route, _router, _boardNoAuth, _tagService, _authService, _formBuilder, _hotelService) {
        var _this = this;
        this.route = route;
        this._router = _router;
        this._boardNoAuth = _boardNoAuth;
        this._tagService = _tagService;
        this._authService = _authService;
        this._formBuilder = _formBuilder;
        this._hotelService = _hotelService;
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
        this.hotelSearchControl = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required]));
        this.icons = [
            'hotel',
            'coffee',
            'train',
            'food'
        ];
        this.snippetForm = _formBuilder.group({
            "text": ["", [forms_1.Validators.required, forms_1.Validators.minLength(4)]],
            "title": ["", [forms_1.Validators.required, forms_1.Validators.minLength(10)]],
            "image": [""]
        });
        this.hotels = this.hotelSearchControl.valueChanges
            .debounceTime(400)
            .distinctUntilChanged()
            .switchMap(function (term) { return _this._hotelService.getHotels(term); });
    }
    //On page Actions
    BoardSingleComponent.prototype.likeBoard = function (boardId) {
        var data = {
            'requestingUserId': this.userInfo.id,
            'boardId': boardId
        };
        this._boardNoAuth.likeBoard(data);
    };
    BoardSingleComponent.prototype.addSnippet = function (boardId) {
        console.log('boardId:', boardId);
        var data = {
            "boardId": boardId,
            "snippet": {
                "text": this.snippetForm.value.text,
                "title": this.snippetForm.value.title,
                "image": this.snippetForm.value.image
            },
        };
        this._boardNoAuth.addSnippet(data);
        console.log(data);
    };
    ;
    BoardSingleComponent.prototype.addHotel = function (boardId, hotel) {
        console.log("board ID:", boardId);
        console.log('hotel', hotel);
        var addHotel = {
            "hotels": [
                {
                    "boardId": boardId,
                    "hotelId": hotel.hotelId,
                    "hotelName": hotel.hotelName,
                    "hotelDescription": hotel.hotelDescription,
                    "hotelURL": hotel.hotelURL,
                    "hotelImage": hotel.hotelImage,
                    "hotelRating": hotel.hotelRating
                }
            ],
            "boardId": boardId,
            "requestingUserId": this.userInfo.id
        };
        this._boardNoAuth.addHotel(addHotel);
    };
    BoardSingleComponent.prototype.followTag = function (us) {
        var data = {
            'id': this.userInfo.id,
            'tagId': us,
        };
        var content = data;
        this._tagService.followTag(content)
            .subscribe(function (data) {
            console.log('subscribe:', data);
        }, function (error) { return console.log(error); });
    };
    ;
    // On init.
    BoardSingleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var id = +params['id']; // (+) converts string 'id' to a number
            _this.detailBoard$ = _this._boardNoAuth.board$;
            _this._boardNoAuth.getBoardDetailed(id);
            console.log('the response:', _this.detailBoard$);
        });
    };
    BoardSingleComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    // Routes
    BoardSingleComponent.prototype.goToUser = function ($event) {
        this._router.navigate(['/user', $event]);
    };
    BoardSingleComponent.prototype.goToTag = function ($event) {
        this._router.navigate(['/tag', $event]);
    };
    BoardSingleComponent.prototype.isAuth = function () {
        return this._authService.isAuthenticated();
    };
    BoardSingleComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'board-single',
            templateUrl: 'board-single.component.html',
            styleUrls: ['board-single.component.css'],
            directives: [router_1.ROUTER_DIRECTIVES, ng_semantic_1.SEMANTIC_COMPONENTS, ng_semantic_1.SEMANTIC_DIRECTIVES, forms_1.REACTIVE_FORM_DIRECTIVES],
            providers: [board_no_Auth_service_1.BoardNoAuthService, tag_service_1.TagService, hotel_service_1.HotelService]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, board_no_Auth_service_1.BoardNoAuthService, tag_service_1.TagService, auth_service_1.AuthService, forms_1.FormBuilder, hotel_service_1.HotelService])
    ], BoardSingleComponent);
    return BoardSingleComponent;
}());
exports.BoardSingleComponent = BoardSingleComponent;
