import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute }   from '@angular/router';
import { BoardService }  from '../board.service';
import { Observable } from 'rxjs/Observable';
import { Board} from '../board';
import { TagService} from '../../tags/tag.service';
import { AuthService} from '../../auth/auth.service';
import { HotelService} from '../../hotels/hotel.service';

import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  FormArray
} from "@angular/forms";

@Component({
  selector: 'board-single',
  templateUrl: 'board-single.component.html',
  styleUrls: ['board-single.component.css'],
})


export class BoardSingleComponent implements OnInit {

  private sub: any;
  private detailBoard$: Observable<Board[]> ;

  userInfo = JSON.parse(localStorage.getItem('auth_key'));

  hotelSearchControl: FormControl = new FormControl("",Validators.compose([Validators.required]));

  hotels: Observable<any>;

  icons = [
      'hotel',
      'coffee',
      'train',
      'food'
    ];

  snippetForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private _boardService: BoardService,
    private _tagService: TagService,
    private _authService: AuthService,
    private _formBuilder: FormBuilder,
    private _hotelService: HotelService

  ) {

    this.snippetForm = _formBuilder.group({
        "text": ["", [Validators.required, Validators.minLength(4)]],
        "title": ["",[Validators.required, Validators.minLength(10)]],
        "image": [""]
    });

    this.hotels = this.hotelSearchControl.valueChanges
                     .debounceTime(400)
                     .distinctUntilChanged()
                     .switchMap(
                       term => this._hotelService.getHotels(term)
                     );

   }


  //On page Actions

  likeBoard(boardId){
    const data = {
      'requestingUserId': this.userInfo.id,
      'boardId': boardId
    }

    this._boardService.likeBoard(data)
  }

  addSnippet(boardId){
    console.log('boardId:', boardId);

    const data = {
      "boardId": boardId,
      "snippet":{
          "text": this.snippetForm.value.text,
          "title": this.snippetForm.value.title,
          "image": this.snippetForm.value.image
      },
    }
    this._boardService.addSnippet(data)
    console.log(data);
  };

  addHotel(boardId, hotel){
    console.log("board ID:", boardId)
    console.log('hotel', hotel);
    let addHotel = {
            "hotels":[
                {
                "boardId": boardId,
                "hotelId": hotel.hotelId,
                "hotelName": hotel.hotelName,
                "hotelDescription": hotel.hotelDescription,
                "hotelURL": hotel.hotelURL,
                "hotelImage": hotel.hotelImage,
                "hotelRating": hotel.hotelRating
              }
            ],
            "boardId": boardId,
            "requestingUserId": this.userInfo.id
          }

      this._boardService.addHotel(addHotel);

  }

  followTag(us){
      const data = {
        'id': this.userInfo.id,
        'tagId':us,
      };
      const content = data;
      this._tagService.followTag(content)
          .subscribe (
            data => {
            console.log('subscribe:', data);
          },
          error=> console.log(error)
        )
  };

  // On init.
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let id = +params['id']; // (+) converts string 'id' to a number
      this.detailBoard$ = this._boardService.board$;
      this._boardService.getBoardDetailed(id)
      console.log('the response:', this.detailBoard$);
    });

  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  // Routes

  goToUser($event){
      this._router.navigate(['/user', $event]);
  }

  goToTag($event){
     this._router.navigate(['/tag', $event])
  }

  isAuth(){
    return this._authService.isAuthenticated();
  }



}
