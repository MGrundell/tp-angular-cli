"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Hotel service is now on port 9020.
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var http_2 = require('@angular/http');
var router_1 = require('@angular/router');
require('rxjs/add/operator/map');
require('rxjs/add/operator/debounceTime');
require('rxjs/add/operator/distinctUntilChanged');
require('rxjs/add/operator/switchMap');
var HotelService = (function () {
    function HotelService(_http, jsonp, _router) {
        this._http = _http;
        this.jsonp = jsonp;
        this._router = _router;
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
        this.baseUrl = 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com';
    }
    //hotel-service/hotelsByLocation?location=lisbon
    HotelService.prototype.getHotels = function (term) {
        console.log('location Term', term);
        return this._http.get(this.baseUrl + ":9020/hotel-service/hotelsByLocation?location=" + term).map(function (data) { return data.json().hotels; });
    };
    HotelService.prototype.addHotel = function (term) {
        console.log('Adding Hotel', term);
        var body = JSON.stringify(term);
        console.log('body:', body);
        var headers = new http_2.Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service/addHotelToBoardJSON', body, { headers: headers })
            .map(function (data) { return data.json(); }).subscribe(function (data) {
            console.log('New Hotel Added', data);
        }, function (error) { return console.log(error); });
        ;
    };
    HotelService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_2.Http, http_1.Jsonp, router_1.Router])
    ], HotelService);
    return HotelService;
}());
exports.HotelService = HotelService;
