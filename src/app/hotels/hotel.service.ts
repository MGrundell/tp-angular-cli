// Hotel service is now on port 9020.
import { Injectable } from '@angular/core';
import {URLSearchParams, Jsonp} from '@angular/http';
import {Board} from '../boards/board'
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class HotelService {

  private userInfo = JSON.parse(localStorage.getItem('auth_key'));
  private baseUrl: string;


  constructor(
      private _http: Http,
      private jsonp: Jsonp,
      private _router: Router
    ) {
      this.baseUrl  = 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com';
    }


//hotel-service/hotelsByLocation?location=lisbon

  getHotels(term){
      console.log('location Term', term);
      return this._http.get(`${this.baseUrl}:9020/hotel-service/hotelsByLocation?location=${term}`).map(data => data.json().hotels);
  }

  addHotel(term){
    console.log('Adding Hotel', term)
    const body = JSON.stringify(term);
    console.log('body:', body);
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service/addHotelToBoardJSON', body, {headers: headers})
    .map(data => data.json()).subscribe(
      data => {
        console.log('New Hotel Added', data);
      },
      error => console.log(error)
    );;
  }

}
