export class Hotel {
  id: number;
  category: string;
  name: string;
  description: string;
  image: string;
  hotelId: string;
  hotelName: string;
  hotelDescription: string;
  hotelURL: string;
  hotelImage: string;
  hotelRating: number;
  createdAt: string;

  constructor(id: number, category: string, name: string, description: string, image: string, hotelId: string, hotelName: string, hotelDescription: string, hotelURL: string, hotelImage: string, hotelRating: number, createdAt: string ){
    this.id = id;
    this.category = category;
    this.name = name;
    this.description = description;
    this.image = image;
    this.hotelId = hotelId;
    this.hotelName = hotelName;
    this.hotelDescription = hotelDescription;
    this.hotelURL = hotelURL;
    this.hotelImage = hotelImage;
    this.hotelRating = hotelRating;
    this.createdAt = createdAt;
  }
}
