import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router} from '@angular/router';

import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  FormArray
} from "@angular/forms";

import {AuthService} from './auth.service';

@Component({
  selector: 'tsp-login',
  templateUrl: 'auth.component.html',
})
export class AuthComponent implements OnInit {



  error = false;
  errorMessage = '';

  signInForm: FormGroup;

  emailControl: FormControl = new FormControl("", Validators.compose([Validators.required, Validators.minLength(3)]));

  passwordControl: FormControl = new FormControl("", Validators.compose([Validators.required, Validators.minLength(4)]));

  @Output() userInfoLogin: EventEmitter<any> = new EventEmitter();



  constructor(

    private _authService: AuthService,
    private _formBuilder:FormBuilder,
    private _router: Router
  ) {
    this.signInForm = _formBuilder.group({
      'email': this.emailControl,
      'password': this.passwordControl
    });
   }

  ngOnInit() {
  }

  getLogin(){
    console.log(this.signInForm.value);
    this._authService.getLogin(this.signInForm.value)
    /* .subscribe(
      user =>{
        console.log(user)
        if(user.password == this.singInForm.value.password){
          console.log('true');
          window.localStorage.setItem('auth_key',  JSON.stringify(user));
          this.userInfoLogin.emit(user);
          this._router.navigate(['/user',  user.id ]);
        }
      }
    ); */


  }

  createBoard(){
    this._router.navigate(['/create-board']);
  }
  getAuth(){
   console.log(JSON.parse(localStorage.getItem('auth_key')));
  }

  logOut(){
    localStorage.clear();
  }

}
