import {Injectable, EventEmitter} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {User} from '../users/users';
import { Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';


@Injectable()
export class AuthService {
    private _userLoggedOut = new EventEmitter<any>();
    private baseUrl: string;
    public userInfo: User;
    private myValue: User;
    public _emitter = new EventEmitter<any>();


    private _userAuth$: Subject<User[]>;
    private _userAuthStore: {
      userAuth: User[]
    }

    constructor(
      private _http:Http,
      private _router: Router
      ) {
        this._userAuthStore = { userAuth: [] };
        this._userAuth$ = <Subject<User[]>> new Subject()
    }


    get userAuth$(){
      return this._userAuth$.asObservable();
    }

    getLogin(user: User){
      console.log('user obj passed', user);
      this._http.get('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9003/user-service/userByEmail?email='+ user.email+'' )
        .map(user => user.json())
        .subscribe(data =>{
          console.log('data returned pass:', data.password, 'User pass:', user.password)
            if(user.password == data.password){
              this._userAuthStore.userAuth = [data];
              window.localStorage.setItem('auth_key',  JSON.stringify(data));
              this._userAuth$.next(this._userAuthStore.userAuth);
              this._router.navigate(['/user',  data.id ]);
            }
        });
    }

    getValue() {
       return JSON.parse(localStorage.getItem('auth_key'));
    }

    logOut(){
        localStorage.clear();
        this._userLoggedOut.emit(null);
    }


    getLoggedOutEvent(): EventEmitter<any> {
      return this._userLoggedOut;
    }

    public userObj = JSON.parse(localStorage.getItem('auth_key'));

    get getUser(){
      return this.userObj;
    }

    isAuthenticated(): boolean {
      return localStorage.getItem('auth_key') !== null;
    }

}
