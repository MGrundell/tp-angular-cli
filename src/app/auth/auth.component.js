"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var forms_1 = require("@angular/forms");
var auth_service_1 = require('./auth.service');
var user_no_auth_service_1 = require('../users/user-no-auth.service');
var ng_semantic_1 = require('ng-semantic/ng-semantic');
var AuthComponent = (function () {
    function AuthComponent(_userNoAuth, _authService, _formBuilder, _router) {
        this._userNoAuth = _userNoAuth;
        this._authService = _authService;
        this._formBuilder = _formBuilder;
        this._router = _router;
        this.error = false;
        this.errorMessage = '';
        this.emailControl = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(3)]));
        this.passwordControl = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(4)]));
        this.userInfoLogin = new core_1.EventEmitter();
        this.signInForm = _formBuilder.group({
            'email': this.emailControl,
            'password': this.passwordControl
        });
    }
    AuthComponent.prototype.ngOnInit = function () {
    };
    AuthComponent.prototype.getLogin = function () {
        console.log(this.signInForm.value);
        this._authService.getLogin(this.signInForm.value);
        /* .subscribe(
          user =>{
            console.log(user)
            if(user.password == this.singInForm.value.password){
              console.log('true');
              window.localStorage.setItem('auth_key',  JSON.stringify(user));
              this.userInfoLogin.emit(user);
              this._router.navigate(['/user',  user.id ]);
            }
          }
        ); */
    };
    AuthComponent.prototype.createBoard = function () {
        this._router.navigate(['/create-board']);
    };
    AuthComponent.prototype.getAuth = function () {
        console.log(JSON.parse(localStorage.getItem('auth_key')));
    };
    AuthComponent.prototype.logOut = function () {
        localStorage.clear();
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], AuthComponent.prototype, "userInfoLogin", void 0);
    AuthComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'tsp-login',
            templateUrl: 'auth.component.html',
            directives: [
                router_1.ROUTER_DIRECTIVES,
                forms_1.REACTIVE_FORM_DIRECTIVES,
                ng_semantic_1.SEMANTIC_COMPONENTS,
                ng_semantic_1.SEMANTIC_DIRECTIVES
            ]
        }), 
        __metadata('design:paramtypes', [user_no_auth_service_1.UserNoAuth, auth_service_1.AuthService, forms_1.FormBuilder, router_1.Router])
    ], AuthComponent);
    return AuthComponent;
}());
exports.AuthComponent = AuthComponent;
