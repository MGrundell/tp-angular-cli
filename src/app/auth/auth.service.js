"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/Rx');
var router_1 = require('@angular/router');
var Subject_1 = require('rxjs/Subject');
require('rxjs/add/operator/map');
var AuthService = (function () {
    function AuthService(_http, _router) {
        this._http = _http;
        this._router = _router;
        this._userLoggedOut = new core_1.EventEmitter();
        this._emitter = new core_1.EventEmitter();
        this.userObj = JSON.parse(localStorage.getItem('auth_key'));
        this._userAuthStore = { userAuth: [] };
        this._userAuth$ = new Subject_1.Subject();
    }
    Object.defineProperty(AuthService.prototype, "userAuth$", {
        get: function () {
            return this._userAuth$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    AuthService.prototype.getLogin = function (user) {
        var _this = this;
        console.log('user obj passed', user);
        this._http.get('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9003/user-service/userByEmail?email=' + user.email + '')
            .map(function (user) { return user.json(); })
            .subscribe(function (data) {
            console.log('data returned pass:', data.password, 'User pass:', user.password);
            if (user.password == data.password) {
                _this._userAuthStore.userAuth = [data];
                window.localStorage.setItem('auth_key', JSON.stringify(data));
                _this._userAuth$.next(_this._userAuthStore.userAuth);
                _this._router.navigate(['/user', data.id]);
            }
        });
    };
    AuthService.prototype.getValue = function () {
        return JSON.parse(localStorage.getItem('auth_key'));
    };
    AuthService.prototype.logOut = function () {
        localStorage.clear();
        this._userLoggedOut.emit(null);
    };
    AuthService.prototype.getLoggedOutEvent = function () {
        return this._userLoggedOut;
    };
    Object.defineProperty(AuthService.prototype, "getUser", {
        get: function () {
            return this.userObj;
        },
        enumerable: true,
        configurable: true
    });
    AuthService.prototype.isAuthenticated = function () {
        return localStorage.getItem('auth_key') !== null;
    };
    AuthService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, router_1.Router])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;
