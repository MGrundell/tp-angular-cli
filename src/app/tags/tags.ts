export class Tag {
  tagId: string;
  tagName: string;
  tagDescription: string;

  constructor(tagId: string, tagName: string, tagDescription: string){
    this.tagId = tagId;
    this.tagName = tagName;
    this.tagDescription = tagDescription;
  }

}
