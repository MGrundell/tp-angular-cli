import { Injectable} from '@angular/core';
import { Tag} from './tags';
import { Http, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Board} from '../boards/board';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Injectable()

export class TagService{

  public userInfo = JSON.parse(localStorage.getItem('auth_key'));

  private _boards$: Subject<Board[]>;
  private baseUrl: string;
  private dataStore: {  // This is where we will store our data in memory
    boards: Board[]
  };

  constructor(private _http: Http){
    this.baseUrl = 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com';
    this.dataStore = { boards: [] };
    this._boards$ = <Subject<Board[]>>new Subject()
  }

  get board$() {
    return this._boards$.asObservable();
  }

  followTag(content: any){
     const body = JSON.stringify(content);
     const headers = new Headers();
     console.log('This is body content ', body);
     headers.append('Content-Type', 'application/json');
     return this._http.post( `${this.baseUrl}:9003/user-service/addInterestTagIdJSON`, body, {headers: headers})
      .map(data => data.json());
  }

  getBoards(tagId: string){
    // User - signed on

    if(this.userInfo) {
      this._http.get(`${this.baseUrl}:9000/board-service/taggedBoards?tagId=${tagId}&requestingUserId=${this.userInfo.id}`).map(response => response.json()).subscribe(data => {
        this.dataStore.boards = data;
        this._boards$.next(this.dataStore.boards);
      });
    }
  // No user
   if(this.userInfo == null){
    console.log('calling boards with tag id ', tagId);
     this._http.get(`${this.baseUrl}:9000/board-service/taggedBoards?tagId=${tagId}`).map(response => response.json()).subscribe(data => {
       this.dataStore.boards = data;
       this._boards$.next(this.dataStore.boards);
     });
   }
  }

  getAllTags(){
    return this._http.get('http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9007/tag-service/allTags')
                .map(allTags => allTags.json());
  }

  getTag(index: number){

  }

  getIndexOfTag(tag: Tag){

  }

  insertTag(tag: Tag){

  }

  insertTags(tags: Tag[]){

  }

  updateTag(index: number, item: Tag){

  }

  deleteTag(tag: Tag){

  }

  likeBoard(board) {
    console.log(board)
      const body = JSON.stringify(board);
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this._http.post(`${this.baseUrl}:9000/board-service/likeBoardIdJSON`, body, {headers: headers})
      .map(response => response.json()).subscribe(data => {
              this.dataStore.boards.forEach((board, i) => {
                if (board.id === data.id) {
                  console.log('data updated with', data)
                  console.log('data to update', this.dataStore.boards[i]);
                  this.dataStore.boards[i] = data;
                  console.log('passed data ');
                }
              });
          this._boards$.next(this.dataStore.boards);
        }, error => console.log('Could not update Board.'));
    }



}
