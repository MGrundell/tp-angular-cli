import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router'
//Third Party
import {Board} from '../boards/board';
import {AuthService} from '../auth/auth.service';
import { UserService} from '../users/user.service';
import { Observable } from 'rxjs/Observable';

//Prividers
import {TagService} from './tag.service';
import {BoardTBFService} from '../boards/board-tbf.service';

@Component({
  // moduleId: module.id,
  selector: 'tag-component',
  templateUrl: './tag.component.html',
  styleUrls: ['../boards/board.component.css'],
})
export class TagComponent implements OnInit {
  boards$: Observable<Board[]>;
  userInfo = JSON.parse(localStorage.getItem('auth_key'));
  private sub: any;
  public sort: {field: string, desc: boolean} = {field: 'score', desc: true};

  constructor(
      private _tagService: TagService,
      private route: ActivatedRoute,
      private router: Router,
      private _authService: AuthService,
      private _userService: UserService,
      private _TBFService: BoardTBFService
  ) {  }

  isAuth(){
    return this._authService.isAuthenticated();
  }

  gotoDetail(board: Board) {
      console.log(board.id);
      this.router.navigate(['/board', board.id]);
    }

  goToUser($event) {
    this.router.navigate(['/user', $event]);
  }

  likeBoard(boardId){
    if(this.userInfo){
    const data = {
      'requestingUserId': this.userInfo.id,
      'boardId': boardId
    }
    this._TBFService.likeBoard(data)
  }
}



  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
     let tagId = params['tagId'];
     console.log(tagId);
     this.boards$ = this._TBFService.boards$
     this._TBFService.getBoards(tagId);

    });
  }
}
