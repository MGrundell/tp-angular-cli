import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule }   from '@angular/router';

import { TagComponent }     from './tag.component';

const TagRouting: Routes = [
  {
    path: 'tag/:tagId',
    component: TagComponent,
  },
];

export const TagRoutes = RouterModule.forChild(TagRouting);
