"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/Rx');
var Subject_1 = require('rxjs/Subject');
require('rxjs/add/operator/mergeMap');
require('rxjs/add/operator/map');
require('rxjs/add/operator/cache');
var router_1 = require('@angular/router');
var UserNoAuth = (function () {
    function UserNoAuth(_http, _router) {
        this._http = _http;
        this._router = _router;
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
        this.baseUrl = 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com';
        this.dataStore = { boards: [] };
        this._boards$ = new Subject_1.Subject();
        this.userStore = { users: [] };
        this._user$ = new Subject_1.Subject();
        this.followStore = { users: [] };
        this._followers$ = new Subject_1.Subject();
        this.followingStore = { users: [] };
        this._following$ = new Subject_1.Subject();
    }
    Object.defineProperty(UserNoAuth.prototype, "board$", {
        get: function () {
            return this._boards$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserNoAuth.prototype, "user$", {
        get: function () {
            return this._user$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserNoAuth.prototype, "follow$", {
        get: function () {
            return this._followers$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserNoAuth.prototype, "following$", {
        get: function () {
            return this._following$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    UserNoAuth.prototype.getUser = function (id) {
        var _this = this;
        console.log(id);
        // If is Auth
        if (this.userInfo) {
            this._http.get(this.baseUrl + ":9003/user-service?userId=" + id + "&requestingUserId=" + this.userInfo.id)
                .map(function (response) { return response.json(); })
                .subscribe(function (data) {
                console.log('Current User', _this.userInfo.id);
                console.log('User Object', data);
                _this.userStore.users = [data];
                _this._user$.next(_this.userStore.users);
            }, function (error) { return console.log('Could not load todos.'); });
        }
        // If is NOT Auth
        if (this.userInfo == null) {
            console.log('using no auth service user ID ');
            this._http.get(this.baseUrl + ":9003/user-service?userId=" + id)
                .map(function (response) { return response.json(); })
                .subscribe(function (data) {
                _this.userStore.users = [data];
                _this._user$.next(_this.userStore.users);
            }, function (error) { return console.log('Could not load todos.'); });
        }
    };
    UserNoAuth.prototype.getBoardsUser = function (id) {
        var _this = this;
        console.log(id);
        if (this.userInfo) {
            this._http.get(this.baseUrl + ":9000/board-service/boardsById?userId=" + id + "&requestingUserId=" + this.userInfo.id).map(function (response) { return response.json(); }).subscribe(function (data) {
                _this.dataStore.boards = data;
                _this._boards$.next(_this.dataStore.boards);
            }, function (error) { return console.log('Could not load todos.'); });
        }
        if (this.userInfo == null) {
            this._http.get(this.baseUrl + ":9000/board-service/boardsById?userId=" + id).map(function (response) { return response.json(); }).subscribe(function (data) {
                _this.dataStore.boards = data;
                _this._boards$.next(_this.dataStore.boards);
            }, function (error) { return console.log('Could not load todos.'); });
        }
    };
    UserNoAuth.prototype.getFollowing = function (id) {
        var _this = this;
        if (this.userInfo) {
            this._http.get(this.baseUrl + ":9003/user-service/followingId?id=" + id + "&requestingUserId=" + this.userInfo.id)
                .map(function (response) { return response.json(); })
                .subscribe(function (data) {
                console.log(data);
                _this.followingStore.users = data;
                _this._following$.next(_this.followingStore.users);
            }, function (error) { return console.log('Could not load todos.'); });
        }
        if (this.userInfo == null) {
            this._http.get(this.baseUrl + ":9003/user-service/followingId?id=" + id)
                .map(function (response) { return response.json(); })
                .subscribe(function (data) {
                _this.followingStore.users = data;
                _this._following$.next(_this.followingStore.users);
            }, function (error) { return console.log('Could not load todos.'); });
        }
    };
    UserNoAuth.prototype.getFollowers = function (id) {
        var _this = this;
        this._http.get(this.baseUrl + ":9003/user-service/followingMeId?id=" + id)
            .map(function (response) { return response.json(); })
            .subscribe(function (data) {
            console.log(data);
            _this.followStore.users = data;
            _this._followers$.next(_this.followStore.users);
        }, function (error) { return console.log('Could not load todos.'); });
    };
    UserNoAuth.prototype.signUp = function (data) {
        var body = JSON.stringify(data);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        console.log(body);
        return this._http.post(this.baseUrl + ":9003/user-service/addUserJSON", body, { headers: headers })
            .map(function (data) { return data.json(); });
    };
    UserNoAuth.prototype.likeBoard = function (board) {
        var _this = this;
        console.log(board);
        var body = JSON.stringify(board);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        this._http.post(this.baseUrl + ":9000/board-service/likeBoardIdJSON", body, { headers: headers })
            .map(function (response) { return response.json(); }).subscribe(function (data) {
            _this.dataStore.boards.forEach(function (board, i) {
                if (board.id === data.id) {
                    console.log('data updated with', data);
                    console.log('data to update', _this.dataStore.boards[i]);
                    _this.dataStore.boards[i] = data;
                    console.log('passed data ');
                }
            });
            _this._boards$.next(_this.dataStore.boards);
        }, function (error) { return console.log('Could not update Board.'); });
    };
    UserNoAuth.prototype.addTag = function (tags) {
        var _this = this;
        console.log(tags);
        var body = JSON.stringify(tags);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        this._http.post(this.baseUrl + ":9003/user-service/addInterestTagsIdJSON", body, { headers: headers })
            .map(function (response) { return response.json(); })
            .subscribe(function (data) {
            _this.userStore.users.forEach(function (board, i) {
                if (board.id === data.id) {
                    console.log('board:', board);
                    console.log('data:', data);
                    _this.userStore.users[i] = data;
                }
            });
        }, function (error) { return console.log('Could not update user'); });
    };
    UserNoAuth.prototype.followUser = function (content) {
        var _this = this;
        console.log(content);
        var body = JSON.stringify(content);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        this._http.post(this.baseUrl + ":9003/user-service/followUserIdJSON", body, { headers: headers })
            .map(function (response) { return response.json(); })
            .subscribe(function (data) {
            console.log('returned data', data);
            _this.userStore.users.forEach(function (board, i) {
                console.log('userStore board:', board);
                if (board.id === data.id) {
                    _this.userStore.users[i] = data;
                }
            });
        }, function (error) { return console.log('Could not update user'); });
    };
    UserNoAuth = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, router_1.Router])
    ], UserNoAuth);
    return UserNoAuth;
}());
exports.UserNoAuth = UserNoAuth;
