// Angular modules
import { CommonModule } from '@angular/common'
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { BrowserModule  } from '@angular/platform-browser'
import { HttpModule, JsonpModule } from '@angular/http'

// Third Party modules
import { NgSemanticModule } from 'ng-semantic';
import { MasonryModule } from 'angular2-masonry';
// Components
import { UserBoardComponent } from './boards/user-boards.component'
import { UserFollowersComponent } from './followers/user-followers.component'
import { UserFollowingComponent } from './following/user-following.component'
import { UserComponent } from './user.component'

// Services

// Routing
import { UserRouting } from './user.routes';

@NgModule({
  imports: [
    UserRouting,
    FormsModule,
    CommonModule,
    BrowserModule,
    NgSemanticModule,
    MasonryModule
  ],
  declarations: [
      UserComponent,
      UserBoardComponent,
      UserFollowersComponent,
      UserFollowingComponent,
      UserBoardComponent,
  ],
   schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class UserModule {}
