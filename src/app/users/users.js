"use strict";
var User = (function () {
    function User(id, email, password, firstname, lastname, homeCity, homeCountry) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.homeCity = homeCity;
        this.homeCountry = homeCountry;
    }
    return User;
}());
exports.User = User;
