import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import { Board } from '../boards/board';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/cache';
import {Router} from '@angular/router';
import {User} from './users';

@Injectable()
export class UserService{
  setUser$: any
  currentUser$: any
  userEmail$: any
  public userInfo = JSON.parse(localStorage.getItem('auth_key'));

  private _boards$: Subject<Board[]>;
  private baseUrl: string;
  private dataStore: {
    boards: Board[]
  };

  private _user$: Subject<User[]>;
  private userStore: {
    users: User[]
  };

  private _followers$: Subject<User[]>;
  private followStore: {
    users: User[]
  };

  private _following$: Subject<User[]>;
  private followingStore: {
    users: User[]
  };

  constructor(
    private _http: Http,
    private _router: Router

  ){
          this.baseUrl = 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com';

          this.dataStore = { boards: [] };
          this._boards$ = <Subject<Board[]>>new Subject()

          this.userStore = { users: [] };
          this._user$ = <Subject<User[]>> new Subject()

          this.followStore = { users: [] };
          this._followers$ = <Subject<User[]>> new Subject()

          this.followingStore = { users: [] };
          this._following$ = <Subject<User[]>> new Subject()
  }

  get board$() {
    return this._boards$.asObservable();
  }

  get user$() {
    return this._user$.asObservable();
  }

  get follow$() {
    return this._followers$.asObservable();
  }

  get following$() {
    return this._following$.asObservable();
  }

  // Inclution of RequestingUserID
  // -- In preperation for JWT
  // - same services should be invoked with the passing JWT in the future

  userAuth(){
    if(this.userInfo){
        return '&requestingUserId=', this.userInfo.id, ' ';
    }
    if(this.userInfo == null){
      return '';
    }
  }

  getUser(id: any){
      this._http.get(`${this.baseUrl}:9003/user-service?userId=${id}${this.userAuth()}`)
      .map(response => response.json())
        .subscribe(data => {
          this.userStore.users = [data];
          this._user$.next(this.userStore.users);
     },
        error => console.log('Could not load users.'));

    }

  getBoardsUser(id){
      this._http.get(`${this.baseUrl}:9000/board-service/boardsById?userId=${id}${this.userAuth()}`).map(response => response.json()).subscribe(data => {
       this.dataStore.boards = data;
       this._boards$.next(this.dataStore.boards);
     },
     error => console.log('Could not load User Boards.'));
}

  getFollowing(id: number){
      this._http.get(`${this.baseUrl}:9003/user-service/followingId?id=${id}${this.userAuth()}`)
          .map(response => response.json())
          .subscribe(data => {
            console.log(data);
              this.followingStore.users = data;
              this._following$.next(this.followingStore.users);
            },
      error => console.log('Could not load todos.'));
    }

  getFollowers(id: number){
    this._http.get(`${this.baseUrl}:9003/user-service/followingMeId?id=${id}${this.userAuth()}`)
        .map(response => response.json())
        .subscribe(
          data => {
            console.log(data);
            this.followStore.users = data;
            this._followers$.next(this.followStore.users);
          },
          error => console.log('Could not load todos.')
        );
      }

  signUp(data: any){
    const body = JSON.stringify(data);
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return  this._http.post(`${this.baseUrl}:9003/user-service/addUserJSON`, body, {headers: headers})
      .map(data => data.json());
  }

  likeBoard(board) {
      const body = JSON.stringify(board);
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this._http.post(`${this.baseUrl}:9000/board-service/likeBoardIdJSON`, body, {headers: headers})
      .map(response => response.json()).subscribe(data => {
              this.dataStore.boards.forEach((board, i) => {
                if (board.id === data.id) {
                  console.log('data updated with', data)
                  console.log('data to update', this.dataStore.boards[i]);
                  this.dataStore.boards[i] = data;
                  console.log('passed data ');
                }
              });
          this._boards$.next(this.dataStore.boards);
        }, error => console.log('Could not update Board.'));
    }

  addTag(tags: any){
    console.log(tags);
    const body = JSON.stringify(tags);
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this._http.post(`${this.baseUrl}:9003/user-service/addInterestTagsIdJSON`, body, {headers: headers})
    .map(response => response.json())
    .subscribe(data => {
      this.userStore.users.forEach((board, i) =>{
        if (board.id === data.id ){
          this.userStore.users[i] = data;
        }
      });
    }, error => console.log('Could not update userTags'));
  }

  followUser(content: any){
    console.log(content)
    const body = JSON.stringify(content);
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this._http.post(`${this.baseUrl}:9003/user-service/followUserIdJSON`, body, {headers: headers})
    .map(response => response.json())
    .subscribe(data => {
      console.log('returned data', data);
      this.userStore.users.forEach((board, i) =>{
        console.log('userStore board:', board);
        if (board.id === data.id ){
          this.userStore.users[i] = data;
        }
      });
    }, error => console.log('Could not update Following user'));
  }

}
