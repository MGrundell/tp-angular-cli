"use strict";
var user_component_1 = require('./user.component');
var user_following_component_1 = require('./following/user-following.component');
var user_boards_component_1 = require('./boards/user-boards.component');
var user_followers_component_1 = require('./followers/user-followers.component');
exports.UserRoutes = [
    {
        path: 'user/:id',
        component: user_component_1.UserComponent,
        children: [
            {
                path: '',
                component: user_boards_component_1.UserBoardComponent
            },
            {
                path: 'following',
                component: user_following_component_1.UserFollowingComponent
            },
            {
                path: 'followers',
                component: user_followers_component_1.UserFollowersComponent
            }
        ]
    },
];
