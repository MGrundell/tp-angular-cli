import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router'

import { UserService} from '../user.service';
import { Observable } from 'rxjs/Observable';
import { User} from '../users';

@Component({
  selector: 'user-following',
  templateUrl: 'user-following.component.html',
})
export class UserFollowingComponent implements OnInit {
  following: Array<any> = [];
  follow$: Observable<User[]> ;
  userInfo = JSON.parse(localStorage.getItem('auth_key'));
  private sub: any;
  private selectedId: number;

  constructor(
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {  }

  goToUser($event){
    console.log('Should navigate', $event);
    this._router.navigate(['/user', $event]);
  }

  ngOnInit() {
        this._route.parent.params.forEach((params: Params) => {
          let id = +params['id'];
          this.follow$ = this._userService.following$ // subscribe to entire collection
          this._userService.getFollowing(id);
        }
      )
  }
}
