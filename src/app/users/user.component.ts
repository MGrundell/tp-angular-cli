import { Component, OnInit, OnChanges } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router'

import { Observable } from 'rxjs/Observable';
//services
import { UserService } from './user.service';
import { AuthService } from '../auth/auth.service';
import { User } from './users'
import { TagService } from '../tags/tag.service'


@Component({
  selector: 'user-component',
  templateUrl: 'user-component.component.html',
  styleUrls: ['./user-component.component.css'],
})
export class UserComponent implements OnInit {
  users$: Observable<User[]> ;
  userInfo =  JSON.parse(localStorage.getItem('auth_key'));
  associatedTags: Array<any> = [];
  id: any;
  tags: any;
  private sub: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _authService: AuthService,
    private _userService: UserService,
    private _tagService: TagService
  ) {  }

  //Determine user service to use / Auth or not.

  isAuth(){
    return this._authService.isAuthenticated();
  }

  onTagSelect(classId, flag){
    if(flag){
       this.associatedTags.push(classId)
    }
    if(!flag){
      let index = this.associatedTags.indexOf(classId);
      if (index > -1) {
        this.associatedTags.splice(index, 1);
      }
    }
  }

  addInterests(){
    const data = {
      "userId": this.userInfo.id,
      "tags": this.associatedTags
    }
    this._userService.addTag(data);
  }

  onSelect() {
    // Navigate with Absolute link
    console.log('Should nav to following');
  }

  goToTag($event) {
    this.router.navigate(['/tag', $event]);
  }

  goToUser($event) {
      let link = ['User', {id: $event}];
      this.router.navigate(link);
    }


  followers(){
      this.router.navigate(['/user/',this.id, 'followers']);
  }

  following(){
      this.router.navigate(['/user/',this.id, 'following']);
  }

  userBoard(){
    this.router.navigate(['/user/', this.id]);
  }

  followUser(form){
    const data = {
      'id': this.userInfo.id,
      'requestingUserId': this.userInfo.id,
      'followId': form,
    };
    this._userService.followUser(data);
  }

  followTag(tagId){
      const data = {
        'id': this.userInfo.id,
        'tagId': tagId
      };
      const content = data;
      this._tagService.followTag(content)
          .subscribe (
            data => {
            console.log(data);
          },
          error=> console.log(error)
        )
  };

  ngOnChanges(){
    this.userInfo = this._authService.userAuth$;
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
               this.id = +params['id'];
               this.users$ = this._userService.user$;
               this._userService.getUser(this.id)
    });

    this._tagService.getAllTags()
      .subscribe(
          data => {
              this.tags = data;
          },
          error => console.log(error)
      );
  }

  ngOnDestroy () {
    this.sub.unsubscribe();
  }
}
