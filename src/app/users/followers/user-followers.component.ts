import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router'

import { UserNoAuth} from '../user-no-auth.service'
import { Observable } from 'rxjs/Observable';
import { User} from '../users';
import { UserService } from '../user.service';

@Component({
  // moduleId: module.id,
  selector: 'user-follwers',
  templateUrl: '../following/user-following.component.html',
})
export class UserFollowersComponent implements OnInit {
  following: Array<any> = [];
  follow$: Observable<User[]> ;
  userInfo = JSON.parse(localStorage.getItem('auth_key'));
  private sub: any;
  private selectedId: number;

  constructor(
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {  }

  goToUser($event){
    console.log('Should navigate', $event);
    this._router.navigate(['/user', $event]);
  }

  ngOnInit() {

    this._route.parent.params.forEach((params: Params) => {
      let id = +params['id'];
      this.follow$ = this._userService.follow$; // subscribe to entire collection
      this._userService.getFollowers(id);
    });
    /*
    this._router.routerState.parent(this._route).params.subscribe(params => {
          let id = +params['id'];
          this.follow$ = this._userService.follow$;
          this._userService.getFollowers(id);
    }); */
  }
}
