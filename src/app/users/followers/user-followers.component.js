"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var user_no_auth_service_1 = require('../user-no-auth.service');
var UserFollowersComponent = (function () {
    function UserFollowersComponent(_userNoAuth, _route, _router) {
        this._userNoAuth = _userNoAuth;
        this._route = _route;
        this._router = _router;
        this.following = [];
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
    }
    UserFollowersComponent.prototype.goToUser = function ($event) {
        console.log('Should navigate', $event);
        this._router.navigate(['/user', $event]);
    };
    UserFollowersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._router.routerState.parent(this._route).params.subscribe(function (params) {
            var id = +params['id'];
            _this.follow$ = _this._userNoAuth.follow$;
            _this._userNoAuth.getFollowers(id);
        });
    };
    UserFollowersComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'user-follwers',
            templateUrl: '../following/user-following.component.html',
        }), 
        __metadata('design:paramtypes', [user_no_auth_service_1.UserNoAuth, router_1.ActivatedRoute, router_1.Router])
    ], UserFollowersComponent);
    return UserFollowersComponent;
}());
exports.UserFollowersComponent = UserFollowersComponent;
