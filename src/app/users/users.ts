import {Tag} from '../tags/tags';

export class User {
  id: number;
  email: string;
  password: string;
  firstname: string;
  lastname: string;
  homeCity: string;
  homeCountry: string;
  interests: Tag[];

  constructor(id: number, email: string, password: string, firstname: string, lastname: string, homeCity: string, homeCountry: string ){
    this.id = id;
    this.email = email;
    this.password = password;
    this.firstname = firstname;
    this.lastname = lastname;
    this.homeCity = homeCity;
    this.homeCountry = homeCountry;

  }
}
