"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var forms_1 = require("@angular/forms");
var ng_semantic_1 = require("ng-semantic/ng-semantic");
// Appplication Components
var user_no_auth_service_1 = require('../user-no-auth.service');
//TODO: Create, Create board component
//TODO: Create, detailed board View Component
var UserSignup = (function () {
    function UserSignup(_userNoAuth, _router, _formBuilder) {
        this._userNoAuth = _userNoAuth;
        this._router = _router;
        this._formBuilder = _formBuilder;
        this.firstName = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(3)]));
        this.lastName = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(3)]));
        this.city = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(3)]));
        this.country = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(3)]));
        this.gender = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(3)]));
        this.email = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(3)]));
        this.password = new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(3)]));
        this.createUser = _formBuilder.group({
            'firstname': this.firstName,
            'lastname': this.lastName,
            'homeCity': this.city,
            'homeCountry': this.country,
            'gender': this.gender,
            'email': this.email,
            'password': this.password,
            'imageLink': ['http://files.greatermedia.com/uploads/sites/21/2016/03/iggy.jpg'],
        });
    }
    UserSignup.prototype.onSignUp = function () {
        var _this = this;
        console.log(this.createUser.value);
        this._userNoAuth.signUp(this.createUser.value)
            .subscribe(function (data) {
            console.log(data);
            _this.response = JSON.stringify(data);
            _this._router.navigate(['/']);
        }, function (error) { return console.log(error); });
    };
    UserSignup = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: './user-signup.component.html',
            styleUrls: ['./user-signup.component.css'],
            providers: [user_no_auth_service_1.UserNoAuth],
            directives: [
                router_1.ROUTER_DIRECTIVES,
                ng_semantic_1.SEMANTIC_COMPONENTS,
                ng_semantic_1.SEMANTIC_DIRECTIVES,
                forms_1.REACTIVE_FORM_DIRECTIVES
            ]
        }), 
        __metadata('design:paramtypes', [user_no_auth_service_1.UserNoAuth, router_1.Router, forms_1.FormBuilder])
    ], UserSignup);
    return UserSignup;
}());
exports.UserSignup = UserSignup;
