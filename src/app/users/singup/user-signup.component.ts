import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router'
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  FormArray
} from "@angular/forms";

import { SEMANTIC_COMPONENTS, SEMANTIC_DIRECTIVES } from "ng-semantic/ng-semantic";
// Appplication Components
import {UserNoAuth} from '../user-no-auth.service';
import {User} from '../users';
//TODO: Create, Create board component
//TODO: Create, detailed board View Component

@Component({
  // moduleId: module.id,
  templateUrl:'./user-signup.component.html',
  styleUrls: ['./user-signup.component.css'],
})


export class UserSignup {
  response: string;

  createUser: FormGroup;

  firstName: FormControl = new FormControl("", Validators.compose([Validators.required, Validators.minLength(3)]));

  lastName: FormControl = new FormControl("", Validators.compose([Validators.required, Validators.minLength(3)]));

  city: FormControl = new FormControl("", Validators.compose([Validators.required, Validators.minLength(3)]));

  country: FormControl = new FormControl("", Validators.compose([Validators.required, Validators.minLength(3)]));

  gender: FormControl = new FormControl("", Validators.compose([Validators.required, Validators.minLength(3)]));

  email: FormControl = new FormControl("", Validators.compose([Validators.required, Validators.minLength(3)]));

  password: FormControl = new FormControl("", Validators.compose([Validators.required, Validators.minLength(3)]));

  constructor(
      private _userNoAuth: UserNoAuth,
      private _router: Router,
      private _formBuilder: FormBuilder
      ){

      this.createUser = _formBuilder.group({
        'firstname':  this.firstName,
        'lastname':   this.lastName,
        'homeCity':       this.city,
        'homeCountry':    this.country,
        'gender':     this.gender,
        'email':      this.email,
        'password':   this.password,
        'imageLink':  ['http://files.greatermedia.com/uploads/sites/21/2016/03/iggy.jpg'],
      })
  }

  onSignUp(){
      console.log(this.createUser.value);
      this._userNoAuth.signUp(this.createUser.value)
        .subscribe (
            data => {
            console.log(data);
            this.response = JSON.stringify(data);
            this._router.navigate(['/'])
          },
          error=> console.log(error)
        )
  }

}
