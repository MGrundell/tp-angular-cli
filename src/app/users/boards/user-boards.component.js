"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
//providers
var user_no_auth_service_1 = require('../user-no-auth.service');
var sort_pipe_1 = require('../../boards/sort.pipe');
var auth_service_1 = require('../../auth/auth.service');
var UserBoardComponent = (function () {
    function UserBoardComponent(_userNoAuth, _route, _router, _authService) {
        this._userNoAuth = _userNoAuth;
        this._route = _route;
        this._router = _router;
        this._authService = _authService;
        this.following = [];
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
    }
    ;
    UserBoardComponent.prototype.goToUser = function ($event) {
        console.log('Should navigate', $event);
        this._router.navigate(['/user', $event]);
    };
    UserBoardComponent.prototype.likeBoard = function (boardId) {
        var data = {
            'requestingUserId': this.userInfo.id,
            'boardId': boardId
        };
        this._userNoAuth.likeBoard(data);
    };
    UserBoardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
        this._router.routerState.parent(this._route).params.subscribe(function (params) {
            var id = +params['id'];
            _this.boards$ = _this._userNoAuth.board$; // subscribe to entire collection
            console.log('return user boards:', _this.boards$);
            _this._userNoAuth.getBoardsUser(id);
        });
    };
    UserBoardComponent.prototype.gotoDetail = function (board) {
        this._router.navigate(['/board', board.id]);
    };
    UserBoardComponent.prototype.isAuth = function () {
        return this._authService.isAuthenticated();
    };
    UserBoardComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'user-board',
            templateUrl: '../../tags/tag.component.html',
            styleUrls: ['../../boards/board.component.css'],
            providers: [user_no_auth_service_1.UserNoAuth],
            pipes: [sort_pipe_1.SortyPipe]
        }), 
        __metadata('design:paramtypes', [user_no_auth_service_1.UserNoAuth, router_1.ActivatedRoute, router_1.Router, auth_service_1.AuthService])
    ], UserBoardComponent);
    return UserBoardComponent;
}());
exports.UserBoardComponent = UserBoardComponent;
