import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router'
//providers
import { UserService} from '../user.service';
import { AuthService} from '../../auth/auth.service';
import { Observable } from 'rxjs/Observable';
import {Board} from '../../boards/board';
import {User} from '../users';

@Component({
  selector: 'user-board',
  templateUrl:'../../tags/tag.component.html',
  styleUrls: ['../../boards/board.component.css'],
})

export class UserBoardComponent implements OnInit {
  boards$: Observable<Board[]>;
  following: Array<any> = [];
  userInfo = JSON.parse(localStorage.getItem('auth_key'));;
  private sub: any;
  constructor(
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _authService: AuthService
  ) {  }

  goToUser($event){
    console.log('Should navigate', $event);
    this._router.navigate(['/user', $event]);
  }

  likeBoard(boardId){
    const data = {
      'requestingUserId': this.userInfo.id,
      'boardId': boardId
    }
    this._userService.likeBoard(data)
  }

  ngOnInit() {
    this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
    this._route.params.forEach((params: Params) => {
      let id = +params['id'];
      this.boards$ = this._userService.board$; // subscribe to entire collection
      this._userService.getBoardsUser(id)
    });

    /*this._router.routerState.parent(this._route).params.subscribe(params => {
            let id = +params['id'];
            this.boards$ = this._userService.board$; // subscribe to entire collection
            console.log('Using route:', id, '/boards?')
            console.log('return user boards:', this.boards$)
            this._userService.getBoardsUser(id)
      }); */
  }

  gotoDetail(board) {
    this._router.navigate(['/board', board.id]);
  }

  isAuth(){
    return this._authService.isAuthenticated();
  }
}
