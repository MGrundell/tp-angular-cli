import { ModuleWithProviders }   from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserComponent }     from './user.component';
import { UserFollowingComponent} from './following/user-following.component';
import { UserBoardComponent} from './boards/user-boards.component';
import { UserFollowersComponent} from './followers/user-followers.component';

const UserRoutes: Routes = [
  {
    path: 'user/:id',
    component: UserComponent,
    children: [
      {
        path: '',
        component: UserBoardComponent
      },
      {
        path: 'following',
        component: UserFollowingComponent
      },
      {
        path: 'followers',
        component: UserFollowersComponent
      }
    ]
  },
];

export const UserRouting: ModuleWithProviders = RouterModule.forChild(UserRoutes);
