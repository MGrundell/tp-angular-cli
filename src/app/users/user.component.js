"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var ng_semantic_1 = require("ng-semantic/ng-semantic");
//services
var user_no_auth_service_1 = require('./user-no-auth.service');
var auth_service_1 = require('../auth/auth.service');
var tag_service_1 = require('../tags/tag.service');
var UserComponent = (function () {
    function UserComponent(route, router, _userNoAuth, _authService, _tagService) {
        this.route = route;
        this.router = router;
        this._userNoAuth = _userNoAuth;
        this._authService = _authService;
        this._tagService = _tagService;
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
        this.associatedTags = [];
    }
    UserComponent.prototype.isAuth = function () {
        return this._authService.isAuthenticated();
    };
    UserComponent.prototype.onTagSelect = function (classId, flag) {
        if (flag) {
            this.associatedTags.push(classId);
        }
        if (!flag) {
            var index = this.associatedTags.indexOf(classId);
            if (index > -1) {
                this.associatedTags.splice(index, 1);
            }
        }
    };
    UserComponent.prototype.addInterests = function () {
        var data = {
            "userId": this.userInfo.id,
            "tags": this.associatedTags
        };
        this._userNoAuth.addTag(data);
    };
    UserComponent.prototype.onSelect = function () {
        // Navigate with Absolute link
        console.log('Should nav to following');
    };
    UserComponent.prototype.goToTag = function ($event) {
        this.router.navigate(['/tag', $event]);
    };
    UserComponent.prototype.goToUser = function ($event) {
        var link = ['User', { id: $event }];
        this.router.navigate(link);
    };
    UserComponent.prototype.followers = function () {
        this.router.navigate(['/user/', this.id, '/followers']);
    };
    UserComponent.prototype.following = function () {
        this.router.navigate(['/user/', this.id, '/following']);
    };
    UserComponent.prototype.userBoard = function () {
        this.router.navigate(['/user/', this.id]);
    };
    UserComponent.prototype.followUser = function (form) {
        var data = {
            'id': this.userInfo.id,
            'requestingUserId': this.userInfo.id,
            'followId': form,
        };
        this._userNoAuth.followUser(data);
    };
    UserComponent.prototype.followTag = function (tagId) {
        var data = {
            'id': this.userInfo.id,
            'tagId': tagId
        };
        var content = data;
        this._tagService.followTag(content)
            .subscribe(function (data) {
            console.log(data);
        }, function (error) { return console.log(error); });
    };
    ;
    UserComponent.prototype.ngOnChanges = function () {
        this.userInfo = this._authService.userAuth$;
        console.log('this is user Inxfo', this.userInfo);
    };
    UserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.users$ = _this._userNoAuth.user$;
            _this._userNoAuth.getUser(_this.id);
        });
        this._tagService.getAllTags()
            .subscribe(function (data) {
            _this.tags = data;
        }, function (error) { return console.log(error); });
    };
    UserComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    UserComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'user-component',
            templateUrl: 'user-component.component.html',
            styleUrls: ['user-component.component.css'],
            directives: [
                router_1.ROUTER_DIRECTIVES,
                ng_semantic_1.SEMANTIC_COMPONENTS,
                ng_semantic_1.SEMANTIC_DIRECTIVES],
            providers: [user_no_auth_service_1.UserNoAuth, tag_service_1.TagService]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, user_no_auth_service_1.UserNoAuth, auth_service_1.AuthService, tag_service_1.TagService])
    ], UserComponent);
    return UserComponent;
}());
exports.UserComponent = UserComponent;
