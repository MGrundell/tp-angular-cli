import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NgModuleFactoryLoader } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule,  JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NgSemanticModule } from 'ng-semantic';
import { MasonryModule } from 'angular2-masonry';
// routing
import { routing, appRoutingProviders }    from './app.routes';
import { Jsonp } from '@angular/http';
/* Services */
import { AuthService } from './auth/auth.service';
import { BoardService} from './boards/board.service';
import { BoardTBFService} from './boards/board-tbf.service';
import { TagService } from './tags/tag.service';
import { Photo } from './search/flickr-search.service';
import { HotelService } from './hotels/hotel.service';
import { UserService } from './users/user.service';
import { SearchService } from './search/search.service';
/* Navigation */
import { NavigationComponent } from './navigation/navigation.component';
import { InputDebounceComponent } from './search/search-input.component';
import { AuthComponent } from './auth/auth.component';
import { SearchComponent } from './search/search.component';

/* Boards */
import { BoardModule } from  './boards/board.module';

/* User */
import { UserModule } from './users/user.module';
import { TagComponent } from './tags/tag.component';
import { AsyncNgModuleLoader } from './async-ng-module-loader';
import { UserSignup } from './users/singup/user-signup.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    AuthComponent,
    InputDebounceComponent,
    SearchComponent,
    TagComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    NgSemanticModule,
    MasonryModule,
    routing,
    UserModule,
    BoardModule
  ],
  providers: [
    BoardService,
    BoardTBFService ,
    AuthService,
    UserService,
    TagService,
    Photo,
    HotelService,
    SearchService,
    {provide: NgModuleFactoryLoader, useClass: AsyncNgModuleLoader}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
