"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var ng_semantic_1 = require("ng-semantic/ng-semantic");
var search_input_component_1 = require('../search/search-input.component');
var auth_component_1 = require('../auth/auth.component');
var auth_service_1 = require('../auth/auth.service');
var NavigationComponent = (function () {
    function NavigationComponent(_elRef, _router, _authService) {
        this._elRef = _elRef;
        this._router = _router;
        this._authService = _authService;
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
    }
    NavigationComponent.prototype.searchChanged = function (value) {
        console.log('Empty Array', value);
        if (value == '') {
            this._router.navigate[''];
        }
        if (value !== null) {
            var link = ['/search', value];
            this._router.navigate(link);
        }
    };
    NavigationComponent.prototype.userInfoLogin = function ($event) {
        this.userInfo = $event;
        console.log('this is event emitter', this.userInfo);
    };
    NavigationComponent.prototype.ngOnDestroy = function () {
        this.userInfo.dispose();
    };
    NavigationComponent.prototype.ngOnInit = function () {
        console.log(this.userInfo);
        this.userInfo = this._authService.getUser;
    };
    NavigationComponent.prototype.ngOnChanges = function () {
        this.userInfo = this._authService.getUser;
        console.log(this.userInfo);
    };
    NavigationComponent.prototype.goToUser = function ($event) {
        this._router.navigate(['/user', $event]);
    };
    NavigationComponent.prototype.isAuth = function () {
        // console.log('is Authenticated', this._authService.isAuthenticated())
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
        return this._authService.isAuthenticated();
    };
    NavigationComponent.prototype.signUp = function () {
        this._router.navigate(['/signUp']);
    };
    NavigationComponent.prototype.createBoard = function () {
        console.log('false');
        //this._router.navigate(['CreateBoard']);
    };
    NavigationComponent.prototype.logOut = function () {
        this._authService.logOut();
        this._router.navigate(['/']);
    };
    NavigationComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'tps-navigation',
            templateUrl: 'head-nav.component.html',
            directives: [
                search_input_component_1.InputDebounceComponent,
                ng_semantic_1.SEMANTIC_COMPONENTS,
                ng_semantic_1.SEMANTIC_DIRECTIVES,
                router_1.ROUTER_DIRECTIVES,
                auth_component_1.AuthComponent
            ],
            styleUrls: ['navigation.component.css'],
            outputs: ['searchNav']
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, router_1.Router, auth_service_1.AuthService])
    ], NavigationComponent);
    return NavigationComponent;
}());
exports.NavigationComponent = NavigationComponent;
