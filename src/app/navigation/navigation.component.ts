import { Component,  OnInit, OnChanges, Input, Output, ElementRef, EventEmitter} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
//import { CORE_DIRECTIVES} from '@angular/common';
import { Observable} from 'rxjs/Rx';

import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'tp-navigation',
  templateUrl: 'head-nav.component.html',
  styleUrls: ['navigation.component.css'],
  outputs: ['searchNav']
})
export class NavigationComponent implements OnInit {
   public user$: any;
   public userInfo =  JSON.parse(localStorage.getItem('auth_key'));

  constructor(
    private _elRef: ElementRef,
    private _router: Router,
    private _authService: AuthService
  ) {}

    public searchChanged(value) {
      console.log('Empty Array', value);
      if(value == ''){
        this._router.navigate['/'];
      }
      if(value !== null){
        let link = ['/search', value];
        this._router.navigate(link);
      }

     }

    userInfoLogin($event) {
        this.userInfo = $event;
        console.log('this is event emitter', this.userInfo);
    }
    ngOnDestroy () {
        this.userInfo.dispose();
    }

    ngOnInit() {
      console.log(this.userInfo)
      //this.userInfo = this._authService.getUser;
    }

    ngOnChanges() {
      this.userInfo = this._authService.getUser;
      console.log(this.userInfo)
    }

    goToUser($event) {
      this._router.navigate(['/user', $event]);
    }


    isAuth(){
      // console.log('is Authenticated', this._authService.isAuthenticated())
      this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
      return this._authService.isAuthenticated();
    }

    signUp(){
      this._router.navigate(['/signUp']);
    }
    createBoard(){
      console.log('false');
      //this._router.navigate(['CreateBoard']);
    }

    logOut(){
      this._authService.logOut()
      this._router.navigate(['/'])
    }
}
