import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {StreamBoardService} from './other.service'
import {Board} from '../boards/board';

@Component({
    moduleId: module.id,
    selector: 'my-other',
    templateUrl: 'other.component.html',
    styleUrls: ['other.component.css'],
    providers: [StreamBoardService]
})
export class OtherComponent {
      boards$: Observable<Board[]>;
      singleBoards$: Observable<Board>;
      userInfo = JSON.parse(localStorage.getItem('auth_key'));
    constructor(
      private _streamBoard: StreamBoardService
    ){

    }
    likeBoard(form){
        const data = {
          'userId': this.userInfo.id,
          'boardId': form
        }
        this._streamBoard.likeBoard(data)

      }

    ngOnInit() {
      this.boards$ = this._streamBoard.board$; // subscribe to entire collection
      this.singleBoards$ = this._streamBoard.board$
                             .map(todos => todos.find(item => item.id === '1'));
                             // subscribe to only one todo

      this._streamBoard.loadAll();    // load all todos
  //    this._streamBoard.load('1');    // load only todo with id of '1'
    }
}
