import { Injectable } from '@angular/core';

import {Board} from '../boards/board'
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

@Injectable()
export class StreamBoardService {
  private _boards$: Subject<Board[]>;
  private baseUrl: string;
  private dataStore: {  // This is where we will store our data in memory
    boards: Board[]
  };

  // Using Angular DI we use the HTTP service
  constructor(private _http: Http) {
    this.baseUrl  = 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com';
    this.dataStore = { boards: [] };
    this._boards$ = <Subject<Board[]>>new Subject();
  }

  get board$() {
    return this._boards$.asObservable();
  }

  loadAll() {
    this._http.get(`${this.baseUrl}:9000/board-service/boards`).map(response => response.json()).subscribe(data => {
      this.dataStore.boards = data;
      this._boards$.next(this.dataStore.boards);
      console.log(this.dataStore.boards);
    },
    error => console.log('Could not load todos.'));
  }
  load(id: any) {
      this._http.get(`${this.baseUrl}:9000/board-service?boardId=${id}`).map(response => response.json()).subscribe(data => {
        let notFound = true;
      }, error => console.log('Could not load todo.'));
  }

  likeBoard(board) {
    console.log(board)
    const body = JSON.stringify(board);
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this._http.post(`${this.baseUrl}:9000/board-service/likeBoardIdJSON`, body, {headers: headers})
      .map(response => response.json()).subscribe(data => {
        console.log('Data returned', data);
        this.dataStore.boards.forEach((board, i) => {
          if (board.id === data.id) {
            this.dataStore.boards[i] = data;
            console.log(this.dataStore.boards[i]);
           }
        });
        this._boards$.next(this.dataStore.boards);
        console.log('liked');
      }, error => console.log('Could not update todo.'));
  }
}
