"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var other_service_1 = require('./other.service');
var OtherComponent = (function () {
    function OtherComponent(_streamBoard) {
        this._streamBoard = _streamBoard;
        this.userInfo = JSON.parse(localStorage.getItem('auth_key'));
    }
    OtherComponent.prototype.likeBoard = function (form) {
        var data = {
            'userId': this.userInfo.id,
            'boardId': form
        };
        this._streamBoard.likeBoard(data);
    };
    OtherComponent.prototype.ngOnInit = function () {
        this.boards$ = this._streamBoard.board$; // subscribe to entire collection
        this.singleBoards$ = this._streamBoard.board$
            .map(function (todos) { return todos.find(function (item) { return item.id === '1'; }); });
        // subscribe to only one todo
        this._streamBoard.loadAll(); // load all todos
        //    this._streamBoard.load('1');    // load only todo with id of '1'
    };
    OtherComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-other',
            templateUrl: 'other.component.html',
            styleUrls: ['other.component.css'],
            providers: [other_service_1.StreamBoardService]
        }), 
        __metadata('design:paramtypes', [other_service_1.StreamBoardService])
    ], OtherComponent);
    return OtherComponent;
}());
exports.OtherComponent = OtherComponent;
