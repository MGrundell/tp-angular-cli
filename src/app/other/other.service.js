"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Subject_1 = require('rxjs/Subject');
require('rxjs/add/operator/map');
var StreamBoardService = (function () {
    // Using Angular DI we use the HTTP service
    function StreamBoardService(_http) {
        this._http = _http;
        this.baseUrl = 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com';
        this.dataStore = { boards: [] };
        this._boards$ = new Subject_1.Subject();
    }
    Object.defineProperty(StreamBoardService.prototype, "board$", {
        get: function () {
            return this._boards$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    StreamBoardService.prototype.loadAll = function () {
        var _this = this;
        this._http.get(this.baseUrl + ":9000/board-service/boards").map(function (response) { return response.json(); }).subscribe(function (data) {
            _this.dataStore.boards = data;
            _this._boards$.next(_this.dataStore.boards);
            console.log(_this.dataStore.boards);
        }, function (error) { return console.log('Could not load todos.'); });
    };
    StreamBoardService.prototype.load = function (id) {
        this._http.get(this.baseUrl + ":9000/board-service?boardId=" + id).map(function (response) { return response.json(); }).subscribe(function (data) {
            var notFound = true;
        }, function (error) { return console.log('Could not load todo.'); });
    };
    StreamBoardService.prototype.likeBoard = function (board) {
        var _this = this;
        console.log(board);
        var body = JSON.stringify(board);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        this._http.post(this.baseUrl + ":9000/board-service/likeBoardIdJSON", body, { headers: headers })
            .map(function (response) { return response.json(); }).subscribe(function (data) {
            console.log('Data returned', data);
            _this.dataStore.boards.forEach(function (board, i) {
                if (board.id === data.id) {
                    _this.dataStore.boards[i] = data;
                    console.log(_this.dataStore.boards[i]);
                }
            });
            _this._boards$.next(_this.dataStore.boards);
            console.log('liked');
        }, function (error) { return console.log('Could not update todo.'); });
    };
    StreamBoardService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], StreamBoardService);
    return StreamBoardService;
}());
exports.StreamBoardService = StreamBoardService;
